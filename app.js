/* eslint-env: node, es6 */
/* eslint strict: 0 */
'use strict';

const PageAnalysis = require('./helpers/page-analysis');

module.exports = PageAnalysis;
