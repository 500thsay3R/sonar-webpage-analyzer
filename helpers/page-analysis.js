/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-console: 0 */
/* eslint arrow-body-style: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-return-assign: 0 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const cheerio = require('cheerio');
const cld = require('cld');
const czechStem = require('czech-stemmer');
const natural = require('natural');
const querystring = require('querystring');
const request = require('request');
const striptags = require('striptags');
const ukrStem = require('ukrstemmer');
const url = require('url');
const validUrl = require('valid-url');
const winston = require('winston');
const xmlParser = require('xml2js').parseString;

const badTokens = require('../lib/invalid-url-tokens');
const commentSelectors = require('../lib/comment-selectors');
const EnhancedSet = require('./enhanced-set');
const Page = require('./page');
const positionRates = require('../lib/position-rates');

const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

/** Class representing an instance of webpage analysis */
class PageAnalysis {
  /**
   * @param parameters - The set of parameters defining an instance of analysis.
   * @param {string[]} parameters.keys - Secondary search keys.
   * @param {Object[]} parameters.pages - A bunch of pages to be tested.
   * @param {!string} parameters.query - The main search key.
   * @param {?string} [parameters.locale=null] - ISO 3166 Alpha-2 country code.
   * @param {string[]} [parameters.inclusions=[]] - The set of plus-words.
   * @param {string[]} [parameters.exclusions=[]] - The set of minus-words.
   */
  constructor(parameters) {
    const { query, locale = null, keys = [], inc = [], exc = [] } = parameters;

    /** Converts either items or set of items to lower case */
    function lowerCase(item) {
      if (typeof item === 'string') {
        return item.toLocaleLowerCase();
      }
      return _.map(item, element => element.toLocaleLowerCase());
    }

    this._exclusions = lowerCase(inc);
    this._excsTokens = new EnhancedSet();
    this._inclusions = lowerCase(exc);
    this._incsTokens = new EnhancedSet();
    this._keywords = lowerCase(keys);
    this._keysTokens = new EnhancedSet();
    this._locale = locale;
    this._pages = _.map(parameters.pages, page => new Page(page));
    this._query = lowerCase(query);
    this._queryRelevance = 50;
    this._queryTokens = new EnhancedSet();
    this._results = [];
  }

  /**
   * Get analysis minus-words.
   * @returns {String[]} The minus-words.
   */
  get exclusions() {
    return this._exclusions;
  }

  /**
   * Set tokenized version of minus-words.
   * @param {string[]} words - The minus-words to store.
   */
  set excsTokens(words) {
    this._excsTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get tokenized versions of minus-words.
   * @returns {string[]} Tokenized minus-words.
   */
  get excsTokens() {
    return [...this._excsTokens];
  }

  /**
   * Get analysis plus-words.
   * @returns {String[]} The plus-words.
   */
  get inclusions() {
    return this._inclusions;
  }

  /**
   * Set tokenized version of plus-words.
   * @param {string[]} words - The plus-words to store.
   */
  set incsTokens(words) {
    this._incsTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get tokenized versions of plus-words.
   * @returns {string[]} Tokenized plus-words.
   */
  get incsTokens() {
    return [...this._incsTokens];
  }

  /**
   * Get analysis additional keywords.
   * @returns {string[]} Additional keywords.
   */
  get keys() {
    return this._keywords;
  }

  /**
   * Set tokenized version of additional search keys.
   * @param {string[]} words - The additional search keys to store.
   */
  set keysTokens(words) {
    this._keysTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get tokenized versions of additional search keys.
   * @returns {string[]} Tokenized additional search keys.
   */
  get keysTokens() {
    return [...this._keysTokens];
  }

  /**
   * Get analysis locale settings.
   * @returns {?string} - ISO 3166 Alpha-2 country code.
   */
  get locale() {
    return this._locale;
  }

  /**
   * Get main search key.
   * @returns {string} The main search key.
   */
  get query() {
    return this._query;
  }

  /**
   * Set analysis search key's relevancy rate.
   * @param {number} value - Calculated search key's relevance
   */
  set queryRelevance(value) {
    this._queryRelevance = value;
  }

  /**
   * Get relevance rate of the main search key.
   * @returns {number} Calculated relevance.
   */
  get queryRelevance() {
    return Math.round(this._queryRelevance);
  }

  /**
   * Set tokenized version of the main search key.
   * @param {string[]} words - Query tokens.
   */
  set queryTokens(words) {
    this._queryTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get main search query tokens.
   * @returns {string[]} - Main search key tokens.
   */
  get queryTokens() {
    return [...this._queryTokens];
  }

  set results(item) {
    this._results.push(item);
  }

  get results() {
    return this._results;
  }

  /**
   * Fetch the webpage contents.
   * @static
   * @param  {!string} uri - URL to to fetchPage page contents from.
   * @returns {Promise.<Object>} which resolves to { body: {string}, headers: {Object}} containing
   *                             page body and response headers.
   * @rejects if invalid or non-processable URL provided.
   */
  static fetchPage(uri) {
    return new Promise((resolve, reject) => {
      const isGoodUrl = !(_.some(badTokens, token => uri.indexOf(token) !== -1));

      // handle incorrect input urls that can't be processed
      assert(validUrl.isWebUri(uri) && isGoodUrl, `@PAGE-INITIALIZATION :: Invalid url -> ${uri}`);

      const requestOptions = {
        url: uri,
        method: 'GET',
        timeout: 10000,
        rejectUnauthorized: false,
        headers: {
          'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 ' +
          'Firefox/46.0',
        },
      };

      request(requestOptions, (error, response) => {
        if (!error && response && response.headers && response.body) {
          resolve({ body: response.body.toString(), headers: response.headers });
        } else {
          if (requestOptions.url.indexOf('data.alexa.com/data?') === -1) {
            logger.error(`@FETCH-PAGE :: Failed to fetch page contents from ${uri} -> [${error}]`);
          }

          reject(new Error('Failed to fetch page contents'));
        }
      });
    });
  }

  /**
   * Look for comment boxes by their default selector - either id- or class-property.
   * @param {!string} $ - **Cheerio-processed** HTML-code to be tested.
   * @param {!string} provider - Comment box provider's name.
   * @param {!string} selector - The selector rule to be used.
   * @returns {Promise.<?string>} Either comment box provider's name or null-value.
   * @private
   * @static
   */
  static _checkBySelector($, selector, provider) {
    return Promise.resolve(
      ($(selector).length)
        ? provider
        : null
    );
  }

  /**
   * Look for comment boxes by parsing their distinguishing keyTokens that ain't related to CSS.
   * @param {!string} html - String representations of HTML-code to be tested.
   * @param {!string} provider - Comment box provider's name that is being tested.
   * @param {!string} token - Some token to be looked for.
   * @returns {Promise.<?String>}
   * @private
   * @static
   */
  static _checkByString(html, token, provider) {
    return Promise.resolve(
      (html.indexOf(token) !== -1)
        ? provider
        : null
    );
  }

  /**
   * Search for vendor-provided comment boxes.
   * @param {Page} page - The page to be searched for comment boxes.
   * @returns {Promise}
   * @private
   */
  _findVendorComments(page) {
    const $ = cheerio.load(page.body);
    const selectors = _.keys(commentSelectors.vendor.selectors);
    const tags = _.keys(commentSelectors.vendor.tags);

    // form queue of selectors check tasks
    const tasks = _.map(selectors, sel => {
      const selProvider = commentSelectors.vendor.selectors[sel];
      return PageAnalysis._checkBySelector($, sel, selProvider);
    });

    // add tags check tasks to processing queue
    _.forEach(tags, tag => {
      const tagProvider = commentSelectors.vendor.tags[tag];
      return tasks.push(PageAnalysis._checkByString(page.body, tag, tagProvider));
    });

    return Promise.all(tasks)
      .then(found => {
        const boxes = _.compact(found);
        if (boxes.length) {
          logger.info(`@VENDOR-COMMENTS :: ${page.url} -> [${boxes}] detected.`);
        }

        page.commentBoxes = boxes;
      });
  }

  /**
   * Search for non-vendor comment boxes.
   * @param {Page} page - The page to be scanned for comment boxes.
   * @returns {Promise}
   * @private
   */
  _findInternalComments(page) {
    const $ = cheerio.load(page.body);
    const selectors = commentSelectors.intrinsic;
    const tasks = _.map(selectors, s => PageAnalysis._checkBySelector($, s, 'internal'));

    return Promise.all(tasks)
      .then(found => page.commentBoxes = _.compact(found));
  }

  /**
   * Perform parallel vendor & non-vendor comment boxes search.
   * @param {Page} page - The page to be processed.
   * @returns {Promise}
   * @private
   */
  _findComments(page) {
    return Promise.all([
      this._findVendorComments(page),
      this._findInternalComments(page),
    ]);
  }

  /**
   * Retrieve webpage's meta information.
   * @param {Object} $                 -   HTML contents of the webpage processed by cheerio.
   * @param {String} primaryProp       -   The primary meta property selector to get value of.
   * @param {String} [secondaryProp]   -   Alternative meta property selector to get value of.
   * @returns {*} Retrieved value.
   * @private
   */
  _getMeta($, primaryProp, secondaryProp) {
    const value = $(`meta[${primaryProp}]`).attr('content');
    if (value) {
      return value;
    }

    // try once more with the secondary option (if there's any)
    return (secondaryProp)
      ? this._getMeta($, secondaryProp, null)
      : null;
  }

  /**
   * Retrieve necessary data out of page body.
   * @param {Page} page   -   The page to be processed.
   * @returns {Promise.<T>}
   * @private
   */
  _processPageContents(page) {
    const $ = cheerio.load(page.body);

    // parse page title ad-hoc in case it hasn't been provided
    if (!page.title) {
      page.title = $('head title').text();
    }

    // parse page short textual description ad-hoc in case it hasn't been provided
    if (!page.text) {
      page.text = this._getMeta($, 'name="description"') || striptags($('div p').first().text());
    }

    page.author = this._getMeta($, 'name="author"', 'name="creator"');
    page.location = this._getMeta($, 'name="geo.country"', 'name="geo.placename"');
    page.language = $('html').attr('lang') || this._getMeta($, 'http-equiv="content-language"');
    page.date = this._getMeta($, 'name="publication-date"');
    page.xframe = !!page.headers['x-frame-options'] || !!page.headers['X-Frame-Options'];
    page.hashtags = this._getMeta($, 'name="keywords"');

    return Promise.resolve();
  }

  /**
   * Scan for keywords occurrences within some page's HTML-code.
   * @param {string} body - Some chink of webpage to be scanned.
   * @param {string[]} items - Items to look for.
   * @param {string[]} tokenized - Tokenized version of the items to look for.
   * @param {string} type - Denotes what type of items is being looked for.
   * @param {boolean} inTitle - Reveals if it's the title is being scanned
   * @param {boolean} [partial] - Denotes whether full or partial value is to be returned.
   * @returns {Promise}
   * @private
   */
  _scanHits(body, items, tokenized, type, inTitle, partial = false) {
    return new Promise(resolve => {
      const target = body.toLowerCase();
      let result;
      let stake;

      if (type === 'query') {
        // check if there's any occurrence of the `query`-items within webpage
        result = _.every(items, i => target.indexOf(i) !== -1);
        stake = (inTitle) ? 0.4 : 0.15;

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : stake * (1 - 0.3 * +partial) * +result
        );
      } else if (type === 'keys') {
        // count how many keywords of `keys` type are present within the webpage
        result = _.reduce(items, (counter, i) => {
          return (target.indexOf(i) !== -1)
            ? counter++
            : counter;
        }, 0);
        stake = (inTitle) ? 0.35 : 0.1;

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : stake * (1 - 0.3 * +partial) * (+result / items.length)
        );
      } else if (type === 'inclusions') {
        // check if all the `inclusions` are contained within the webpage
        result = _.every(items, i => target.indexOf(i) !== -1);

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : result
        );
      } else {
        // check if none of the `exclusions` are contained within the webpage
        result = _.some(items, i => target.indexOf(i) !== -1);

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : result
        );
      }
    });
  }

  /**
   * Perform parallel search for definitive keys within webpage title and body.
   * @param {string} body           -   The contents of webpage to be scanned.
   * @param {string[]} items        -   Keywords that are searched for.
   * @param {string} title          -   The title of webpage to be scanned.
   * @param {string[]} tokenized    -   Tokenized version of target keywords.
   * @param {string} type           -   Reveals type of keywords that are being searched for.
   * @returns {Promise}
   * @private
   */
  _scanPage(title, body, items, tokenized, type) {
    return Promise.all([
      this._scanHits(title, items, tokenized, type, true),
      this._scanHits(body, items, tokenized, type, false),
    ]);
  }

  /**
   * Evaluate to what extent does some page comply to search request.
   * @param {Page} page - Some page to be tested.
   * @returns {Promise}
   * @private
   */
  _evaluateAccuracy(page) {
    const $ = cheerio.load(page.body);
    const title = $('head title').text();
    const body = striptags($('body').text());

    return Promise.all([
      this._scanPage(title, body, [this.query], this.queryTokens, 'query'),
      this._scanPage(title, body, this.keys, this.keysTokens, 'keys'),
      this._scanPage(title, body, this.inclusions, this.incsTokens, 'inclusions'),
      this._scanPage(title, body, this.exclusions, this.excsTokens, 'exclusions'),
    ]).then(data => {
      const [,, inclusions, exclusions] = data;
      const marks = _.flatten(_.slice(data, 0, 2));

      page.accuracy = _.reduce(marks, (m, rate) => m += rate, 0);

      page.remove = !(
        _.every(inclusions, i => i === true) && _.every(exclusions, i => i === false)
      );
    });
  }

  /**
   * Evaluate query request potency.
   * @param {Page} page
   * @returns {Promise.<T>}
   * @private
   */
  _evaluatePotency(page) {
    const positionIndex = positionRates[page.index] || 0;
    page.potency = (this.queryRelevance + positionIndex * 100) / 2;

    return Promise.resolve();
  }

  /**
   * Define language of the incoming text chunk.
   * @param {string|string[]} chunk - Some item to be tested.
   * @param {string} [hint] - ISO 639-1 language code
   * @see {@link https://github.com/dachev/node-cld#node-cld} for more information on hint.
   * @static
   *
   * @property {Object[]} languages - contains detected languages
   *
   * @returns {Promise.<?string>} Detected language
   */
  static checkLanguage(chunk, hint) {
    return new Promise(resolve => {
      // stringify incoming chunk
      const text = (typeof chunk === 'string')
        ? chunk
        : chunk.join(' ');

      // add tld hint if locale preset has been included
      const options = {};
      if (hint && hint.length) {
        options.tldHint = hint;
      }

      cld.detect(text, options, (error, result) => {
        if (result && !error) {
          // FIXME: remove array compacting when the cld languages bug gets fixed.
          const languages = _.compact(result.languages);
          resolve(languages[0].code || 'en');
        } else {
          resolve(null);
        }
      });
    });
  }

  /**
   * Perform full-blown webpage analysis.
   * @param {Page} page   -   The webpage to be analyzed.
   * @returns {Promise.<?Object>} Analysis results.
   */
  _processPage(page) {
    assert.ok(page.body, '@PROCESS-PAGE :: Empty page body');
    assert.ok(page.url, '@PROCESS-PAGE :: Empty page url');
    assert.ok(page.headers, '@PROCESS-PAGE :: Empty page headers');

    return Promise.all([
      this._findComments(page),
      this._processPageContents(page),
      this._evaluateAccuracy(page),
      this._evaluatePotency(page),
      PageAnalysis.checkLanguage(page.text),
    ])
      .then((results) => {
        const [,,,, lang] = results;

        // if there's no language specified in <html> tag, assign one detected via cld
        if (!page.language) {
          page.language = lang;
        }
      })
      // filter out pages that are subject to removal due to plus- & minus-word presence/absence
      .then(() => {
        if (!page.remove && page.commentBoxes && page.commentBoxes.length &&
          !page.xframe && page.accuracy > 10 && page.potency > 10) {
          this.results = page.contents;
        }
      })
      .catch(error => {
        logger.error('@PAGE-ANALYSIS :: Analysis failed -> REASON:\n', error);
        return null;
      });
  }

  /**
   * Tokenize incoming words and sentences.
   * @param {string|string[]} item - Some standalone word ar a set of words to be tokenized.
   * @param {string} lang - Language of the incoming item.
   * @returns {string|string[]} Stemmed tokens.
   */
  static createTokens(item, lang) {
    // languages supported by natural#PorterStemmer
    const fullySupported = ['it', 'fr', 'es', 'fa', 'pt'];
    // languages supported by natural#Stemmer
    const semiSupported = ['ja', 'pl'];
    let tokenizer;

    if (lang in fullySupported) {
      tokenizer = natural[`PorterStemmer${_.capitalize(lang)}`].tokenizeAndStem;
    } else if (lang in semiSupported) {
      tokenizer = natural[`Stemmer${_.capitalize(lang)}`].tokenizeAndStem;
    } else if (lang === 'en') {
      tokenizer = natural.PorterStemmer.tokenizeAndStem;
    } else if (lang === 'ru') {
      tokenizer = natural.PorterStemmerRu.tokenizeAndStem;
    } else if (lang === 'sv' || lang === 'nn' || lang === 'nb') {
      tokenizer = natural.PorterStemmerNo.tokenizeAndStem;
    } else if (lang === 'uk') {
      tokenizer = ukrStem;
    } else if (lang === 'cs') {
      tokenizer = czechStem;
    } else {
      tokenizer = _.words;
    }

    const text = (typeof item === 'string')
      ? item
      : item.join(' ');

    const results = tokenizer(text);
    logger.debug(`@CREATE-TOKENS :: DONE [${lang}] [${item}] -> ${results}`);
    return results;
  }

  /**
   * Convert parsed Google Trends data to JSON-document.
   * @param {Object} response - Page containing trends data.
   * @param {string} response.body - HTML-contents of the Google Trends data webpage.
   *
   * @returns {Promise} Parsed trends data.
   * @rejects if there's no or empty body provided or if Google Trends server error or ban occurred
   * @static
   * @private
   */
  static _trendsParser(response) {
    const { body } = response;
    return new Promise((resolve, reject) => {
      assert.ok(body, '@TRENDS-PARSER :: Invalid GTrends body');

      /**
       * Scans response body for presence of error messages.
       * @param {string} html         -   Response body to scan.
       * @param {string[]} problems   -   The warnings.
       * @returns {string} detected warning.
       */
      function checkIssues(html, problems) {
        return _.reduce(problems, (res, problem) => {
          if (html.indexOf(problem) !== -1) {
            res = problem;
          }
          return res;
        }, null);
      }

      // check possible Google Trends problem messages
      const issues = [
        'Not enough search volume to show results',
        'Internal Server Error',
        'You have reached your quota limit',
      ];
      const fail = checkIssues(body, issues);

      if (fail && fail.length) {
        reject(new Error(fail));
      } else {
        // omit unnecessary symbols (google returns comments and JS function declaration besides
        // raw data)
        const data = body.substring(62, body.length);

        // split every single json row into two sub-elements, containing date and value respectively
        const dts = data.split('new Date');

        // omit the last two values - they're always null
        const cells = dts.slice(1, dts.length - 3);

        const result = _.map(cells, c => {
          const temp = (c.split('"v":'));

          // sometimes google responds with data that contains \\u2013 symbol - omitting it
          const tempDate = Date.parse(temp[0].split('"')[3].replace('\\u2013 ', ''));
          const value = parseFloat(temp[1]) || 0;

          return { date: new Date(tempDate).toString(), value };
        });

        resolve(result);
      }
    })
      .catch(error => {
        logger.error(`@TRENDS-PARSER :: Failed to retrieve data -> "${error.message}"`);

        if (error.message === 'You have reached your quota limit') {
          logger.info('*** ALERT! *** Service is about to reboot');
          throw new Error('Google ban');
        } else {
          throw new Error('Trends extraction fail');
        }
      });
  }

  /**
   * Extracts trends data from Google Trends database.
   * @param {string} query - The search keyword to get trends for.
   * @param {?string} [country=null] - Defines what country to retrieve trends of.
   *
   * @returns {Promise.<Object>} Google Trends data.
   * @static
   */
  static getTrends(query, country = null) {
    // form Trends request querystring
    const q = {
      hl: 'en-US',                    // set up English as response language
      q: query,                       // keyword to be tested
      cid: 'TIMESERIES_GRAPH_0',      // return time-series graph of keyword trends
      export: 3,                      // export as JSON
      date: 'today 90-d',             // set up representation timeframe to 90 days
    };

    // add country delimiter if the locale-specific analysis is being occurred
    if (country) {
      q.geo = country;
    }

    // form up the URL request
    const trendsUrl = {
      protocol: 'http:',
      host: 'www.google.com',
      pathname: '/trends/fetchComponent',
      search: querystring.stringify(q),
    };

    return PageAnalysis.fetchPage(url.format(trendsUrl))
      .then(contents => PageAnalysis._trendsParser(contents));
  }

  /**
   * Calculate search query relevancy rate.
   * @param {Object[]} trends     -   Google Trends data.
   *
   * @returns {Promise.<number>} The relevancy rate.
   * @private
   */
  _evalRelevanceRate(trends) {
    assert.ok(trends, '@RELEVANCE-RATE :: Missing trends data');
    assert(trends.length > 0, '@RELEVANCE-RATE :: Empty trends data');

    /**
     * Sums up all the elements in the given array.
     * @param {number[]} array    -   The array to be summed up.
     * @returns {number} The sum of elements.
     */
    function wrap(array) {
      return _.reduce(array, (sum, num) => sum += num, 0);
    }

    const yRange = _.map(trends, cell => cell.value);
    while (yRange[yRange.length - 1] === 0) {
      yRange.pop();
    }

    const xRange = _.range(1, yRange.length + 1);

    // formulae that are necessary to make predictions
    const xSampleMean = wrap(xRange) / trends.length;
    const ySampleMean = wrap(yRange) / trends.length;
    const xVarianceRange = _.map(xRange, value => Math.pow(value - xSampleMean, 2));
    const yVarianceRange = _.map(yRange, value => Math.pow(value - ySampleMean, 2));
    const xSigma = Math.sqrt(wrap(xVarianceRange) / trends.length);
    const ySigma = Math.sqrt(wrap(yVarianceRange) / trends.length);

    const covariationRange = [];
    for (let i = 0; i < (yRange.length); i++) {
      covariationRange.push(xRange[i] * yRange[i]);
    }

    const covariationMomentum = wrap(covariationRange) / trends.length - xSampleMean * ySampleMean;
    const covariationCoeff = covariationMomentum / (xSigma * ySigma);

    const prognosis = covariationCoeff * (ySigma / xSigma) * (trends.length - xSampleMean)
      + ySampleMean;
    const actual = yRange[yRange.length - 1];

    let potencyRate = 50;
    potencyRate += (actual > prognosis)
      ? 50 * (1 - prognosis / actual)
      : 50 * (-1) * (1 - actual / prognosis);

    return Promise.resolve(Math.round(potencyRate));
  }

  /**
   * Evaluates relevance of the provided search key.
   * @param {string} country - ISO 3166 Alpha-2 country code to get trends for.
   * @param {string} query - The search key to be tested.
   * @returns {Promise.<number>} Relevancy rate of the given query.
   */
  getQueryRelevance(query, country) {
    return PageAnalysis.getTrends(query, country)
      .then(this._evalRelevanceRate)
      .catch(error => {
        logger.error(`@RELEVANCE-RATE :: Rate evaluation halted -> ${error.message}`);
        return 50;
      });
  }

  /**
   * Set up search parameters by defining language of search keys and tokenize them.
   * @param {string[]} exclusions - The minus-words.
   * @param {string[]} inclusions - The plus-words.
   * @param {string[]} keys - Secondary search keys.
   * @param {string} query - Primary search key
   * @returns {Promise}
   * @private
   */
  _processParameters(query, keys, inclusions, exclusions) {
    const compoundQuery = _.union(keys, inclusions, exclusions);

    return Promise.all([
      // check language of the search-defining keywords
      PageAnalysis.checkLanguage(compoundQuery, this.locale),
      // calculate relevancy of the main search key
      this.getQueryRelevance(this.query, this.locale),
    ])
      .then(data => {
        const [lang, relevance] = data;
        // set up search parameters
        this.keysTokens = PageAnalysis.createTokens(keys, lang);
        this.incsTokens = PageAnalysis.createTokens(inclusions, lang);
        this.excsTokens = PageAnalysis.createTokens(exclusions, lang);
        this.queryTokens = PageAnalysis.createTokens(query, lang);
        this.queryRelevance = relevance;
      });
  }

  /**
   * Parse Alexa Rank XML Ranking results.
   * @param {string} xml - The XML-document to parse.
   * @returns {Promise.<number>} Alexa Rank rating.
   * @private
   */
  _parseRank(xml) {
    return new Promise(resolve => {
      xmlParser(xml, (error, result) => {
        resolve(
          (result)
            ? Number(result.ALEXA.SD[0].POPULARITY[0].$.TEXT)
            : 0
        );
      });
    });
  }

  /**
   * Get Alexa Ranking of the webpage.
   * @param {String} uri - URL of the webpage to gent ranking for,
   * @returns {Promise.<number>} Alexa Rank rating.
   * @private
   */
  _getRanking(uri) {
    const q = {
      cli: 10,
      url: uri,
    };

    const rankUrl = {
      protocol: 'https:',
      host: 'data.alexa.com',
      pathname: '/data',
      search: querystring.stringify(q),
    };

    return PageAnalysis.fetchPage(url.format(rankUrl))
      .then(xml => this._parseRank(xml.body))
      .catch(error => {
        return 0;
      });
  }

  /**
   * Perform page analysis.
   * @param {Page} page - The webpage to be processed.
   * @returns {Promise.<?Object>} Analysis results.
   * @private
   */
  _analyzePage(page) {
    return Promise.all([
      PageAnalysis.fetchPage(page.url),
      this._getRanking(page.channel),
    ])
      .then(data => {
        const [response, rank] = data;

        page.body = response.body;
        page.headers = response.headers;
        page.rank = rank;

        return this._processPage(page);
      })
      .catch(error => {
        return null;
      });
  }

  /**
   * Perform webpage analyses in parallel.
   * @param {Page[]} pages - A set of _pages to be analyzed.
   * @returns {Promise.<Object[]>} Aggregated array of analyses results.
   * @private
   */
  _pageAnalyses(pages) {
    const analyses = _.map(pages, page => this._analyzePage(page));

    return Promise.all(analyses);
  }

  /**
   * Performs leads scan.
   * @returns {Promise.<Object[]>} The set of objects representing leads.
   */
  leadsScan() {
    return this._processParameters(this.query, this.keys, this.inclusions, this.exclusions)
      .then(() => this._pageAnalyses(this._pages))
      .then(() => this.results);
  }
}

module.exports = PageAnalysis;
