/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-console: 0 */
/* eslint arrow-body-style: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-return-assign: 0 */
'use strict';

const _ = require('lodash');
const EnhancedSet = require('./enhanced-set');
const nodeUrl = require('url');
const tools = require('./tools');

/** Class representing a webpage */
class Page {
  /**
   * Instantiate the webpage item.
   * @param {Object} properties - collection of previously defined webpage properties
   * @param {!number} properties.index - Google SERP position of the webpage.
   * @param {!string} properties.url - URL of the webpage.
   * @param {!string} [properties.title=''] - Title of the webpage.
   * @param {!string} [properties.description=''] - Short caption of its content.
   */
  constructor(properties) {
    const { url, title = '', description = '', index } = properties;
    const pageLink = nodeUrl.parse(url);

    this._accuracy = 0;
    this._author = null;
    this._channel = `${pageLink.protocol}//${pageLink.host}`;
    this._comments = new EnhancedSet();
    this._date = null;
    this._headers = null;
    this._html = null;
    this._info = {};
    this._language = null;
    this._location = null;
    this._media = {};
    this._position = index;
    this._potency = 50;
    this._remove = false;
    this._text = tools.shorten(description);
    this._title = tools.shorten(title);
    this._url = url;
    this._xframe = false;
  }

  /**
   * Set accuracy rate for the page.
   * @param {!number} rate - Calculated numeric value of the accuracy rate.
   */
  set accuracy(rate) {
    this._accuracy = Math.round(rate * 100);
  }

  /**
   * Get accuracy rate of the page.
   * @returns {!number} Accuracy rate, number from 0 to 100.
   */
  get accuracy() {
    return this._accuracy;
  }

  /**
   * Set page creator's information
   * @param {?string} name - Page creator's whereabouts.
   */
  set author(name) {
    this._author = tools.prettyPrint(name);
  }

  /**
   * Get author's/creator's info of the page.
   * @returns {?string} Information about page's author/creator.
   */
  get author() {
    return this._author;
  }

  /**
   * Save webpage's HTML-code.
   * @param {string} html - HTML-code pf the webpage.
   */
  set body(html) {
    this._html = html.toString();
  }

  /**
   * Get HTML-code of the webpage.
   * @returns {string} HTML-code.
   */
  get body() {
    return this._html;
  }

  /**
   * Get base URL of the page.
   * @returns {string} Base URL.
   */
  get channel() {
    return this._channel;
  }

  /**
   * Set names of the detected comment boxes' providers.
   * @param {string[]} providers
   */
  set commentBoxes(providers) {
    this._comments.bulkAdd(providers);
  }

  /**
   * Get types of comment boxes integrated to the webpage.
   * @returns {string[]} - Detected comment boxes.
   */
  get commentBoxes() {
    return [...this._comments];
  }

  /**
   * Get detailed webpage info.
   * @returns {Object} Detailed page info.
   */
  get contents() {
    return {
      link: this.url,
      postType: Page.type,
      channel: this._channel,
      author: this.author,
      title: this.title,
      text: this.text,
      index: this.index,
      date: this.date,
      language: this.language,
      location: this.location,
      info: this._info,
      media: this._media,
      xframe: this.xframe,
      potentialRate: this.potency,
      accuracyRate: this.accuracy,
      comments: this.commentBoxes,
    };
  }

  /**
   * Set publication date of the webpage.
   * @param {string} date - The date to save.
   */
  set date(date) {
    if (date) {
      try {
        this._date = new Date(date).toISOString();
      } catch (error) {
        this._date = null;
      }
    }
  }

  /**
   * Get publication date of the webpage.
   * @returns {string} The publication date of the webpage.
   */
  get date() {
    return this._date;
  }

  /**
   * Set associated keywords
   * @param {string} string - meta[keywords] contents.
   */
  set hashtags(string) {
    const temp = (string && string.length)
      ? string
      .replace(', ', ',')
      .split(',')
      : [];

    this._media.hashtags = _.reduce(temp, (acc, item) => {
      if (item.length > 3) {
        acc.push(item.trim().toLocaleLowerCase());
      }
      return acc;
    }, []);
  }

  /**
   * Set page response headers.
   * @param {Object} respHeaders - Response headers to save.
   */
  set headers(respHeaders) {
    this._headers = respHeaders;
  }

  /**
   * Get page response headers.
   * @returns {Object} Page response headers.
   */
  get headers() {
    return this._headers;
  }

  /**
   * Returns SERP of the webpage.
   * @returns {number} SERP of the webpage.
   */
  get index() {
    return this._position;
  }

  /**
   * Set language of the webpage.
   * @param {?string} lang - Detected language.
   */
  set language(lang) {
    if (lang) {
      // omit regional-specific language notation (i.e. 'en-US' -> 'en', 'fr-CA' -> 'fr' etc.)
      this._language = (lang.length <= 2)
        ? lang.toLocaleLowerCase()
        : lang.substring(0, 2).toLocaleLowerCase();
    }
  }

  /**
   * Get language of the webpage.
   * @returns {?string} - ISO 639-1 Alpha 2 language code.
   */
  get language() {
    return this._language;
  }

  /**
   * Set location associated with the webpage.
   * @param {?string} data - The location data to save.
   */
  set location(data) {
    this._location = tools.prettyPrint(data);
  }

  /**
   * Get location data associated with the webpage.
   * @returns {?string} Location data.
   */
  get location() {
    return this._location;
  }

  /**
   * Set potency rate of the request associated with the webpage.
   * @param {number} value - Calculated potency rate.
   */
  set potency(value) {
    this._potency = Math.round(value);
  }

  /**
   * Get potency rate of the request associated with the webpage.
   * @returns {number} The potency rate value.
   */
  get potency() {
    return this._potency;
  }

  /**
   * Set Alexa Rank rate of the webpage.
   * @param {number} ranking - Retrieved Alexa Rank rating value.
   */
  set rank(ranking) {
    this._info.rank = ranking;
  }

  /**
   * Define if the webpage is a subject to removal.
   * @param {boolean} bool - Removal flag.
   */
  set remove(bool) {
    this._remove = bool;
  }

  /**
   * Get removal flag.
   * @returns {boolean} The removal flag.
   */
  get remove() {
    return this._remove;
  }

  /**
   * Set webpage's textual description.
   * @param {string} text - Textual contents of the webpage.
   */
  set text(text) {
    this._text = tools.shorten(text);
  }

  /**
   * Get short page description.
   * @returns {string}
   */
  get text() {
    return this._text;
  }

  /**
   * Set webpage's title.
   * @param {string} text - Textual contents of the webpage.
   */
  set title(text) {
    this._title = tools.shorten(text);
  }

  /**
   * Get short page title.
   * @returns {string}
   */
  get title() {
    return this._title;
  }

  /**
   * Get publication type.
   * @returns {string}
   */
  static get type() {
    return 'text';
  }

  /**
   * Get URL of the page.
   * @returns {string} The URL.
   */
  get url() {
    // remove anchors if there's any
    return this._url.split('#')[0];
  }

  /**
   * Define whether the page can be opened oin iframe.
   * @param {boolean} value - The x-frame flag.
   */
  set xframe(value) {
    this._xframe = value;
  }

  /**
   * Get x-frame flag.
   * @returns {boolean} The flag.
   */
  get xframe() {
    return this._xframe;
  }
}

module.exports = Page;
