/**
 * If any of these tokens is included in some url, it won't be processed
 */
module.exports = [
  '.pdf',
  '.doc',
  'cutestat',
  'clearwebstats',
  'start.cv.ua',
  '.ppt',
  '/download',
];
