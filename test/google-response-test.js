module.exports = [
  {
    index: 2,
    url: 'http://www.xiaoyi.com/en/xiaoyi_en.html',
    title: 'YI Action Camera | XiaoYi Sports Camera - 小蚁运动相机',
    description: 'Lightweight, compact, and durable, the YI Action Camera is your new favorite ' +
    'travel ... Xiaomi Go Pro ... Travel smarter with the YI Action Camera Travel Kit.',
  },
  {
    index: 3,
    url: 'https://www.youtube.com/watch?v=BRagcg7Ucck',
    title: 'Xiaomi Yi vs GoPro Hero HD - YouTube',
    description: '',
  },
  {
    index: 4,
    url: 'https://www.youtube.com/watch?v=_95qhOK1uh0',
    title: 'GoPro Hero4 Session vs Xiaomi Yi Action Camera (HD) - YouTube',
    description: '',
  },
  {
    index: 5,
    url: 'https://itunes.apple.com/us/app/yi-action-yi-action-camera/id963065779?mt=8',
    title: 'YI Action - YI Action Camera on the App Store - iTunes - Apple',
    description: 'Control, preview, and share instantly. Find more ways to play with your YI ' +
    'Action Camera with the YI Action app. The app lets you control and adjust the settings ...',
  },
  {
    index: 6,
    url: 'http://www.cnet.com/products/xiaomi-yi/',
    title: 'Xiaomi Yi review - CNET',
    description: 'The Xiaomi Yi Action Cam\'s video quality and shooting options are well ' +
    'above those of other cameras in its class, but you\'ll need to bring your own accessories.',
  },
  {
    index: 7,
    url: 'http://www.cnet.com/news/get-a-xiaomi-yi-action-camera-for-68-88-shipped/',
    title: 'Get a Xiaomi Yi action camera for $68.88 shipped - CNET',
    description: 'Get a Xiaomi Yi action camera for $68.88 shipped. Before you drop $300-$500' +
    ' on a GoPro, consider this extremely capable competitor. by Rick Broida.',
  },
  {
    index: 8,
    url: 'http://www.yitechnology.com/',
    title: 'YI',
    description: 'YI HOME CAMERA. Connect to your home anytime, anywhere. 720p HD | 111° ' +
    'wide-angle lens | Two-Way Audio | Activity Alerts. Learn More. > Watch Video. >.',
  },
  {
    index: 9,
    url: 'http://www.gearbest.com/action-cameras/pp_153556.html',
    title: 'Original XiaoMi Yi 1080P Ambarella A7LS WIFI Sports Action ...',
    description: 'Just US$72.44 + free shipping, buy Original XiaoMi Yi 1080P Ambarella A7LS' +
    ' WIFI Sports Action Camera CN Version at GearBest.com and enjoy worldwide ...',
  },
  {
    index: 10,
    url: 'http://elproducente.com/xiaomi-yi-accessories-review/',
    title: 'Xiaomi Yi Accessories - Overview & Reviews - el Producente',
    description: 'In this blogpost I want to sum up all Xiaomi Yi Accessories. This article is' +
    ' constantly updated with new products, price comparisons & links to reviews. This post is ...',
  },
  {
    index: 12,
    url: 'http://elproducente.com/underwater-cases-for-xiaomi-yi/',
    title: 'Underwater Cases for Xiaomi Yi - el Producente',
    description: 'Underwater cases for the Xiaomi Yi camera is a hard topic currently as there ' +
    'are many different models available. There is one original case by Xiaomi which ...',
  },
  {
    index: 13,
    url: 'http://www.gizmochina.com/2016/05/11/xiaomi-yi-camera-unboxing-hands-video/',
    title: 'Xiaomi Yi 4K Action Camera Unboxing & Hands On - Gizmochina',
    description: 'Xiaomi officially announced the new generation of the Yi 4K Action Camera today' +
    ' . We already knew a lot about the device thanks to previous leaks, and now, ...',
  },
  {
    index: 14,
    url: 'http://pevly.com/xiaomi-yi-lcd-screen/',
    title: 'Xiaomi YI LCD screen and 2400mah battery | Pevly',
    description: 'Xiaomi YI now has two new interesting accessories : LCD screen (display) and ' +
    'a 2400mah external battery. Here we present to you review and performance ...',
  },
  {
    index: 15,
    url: 'http://www.ixbt.com/divideo/xiaomi-yi.shtml',
    title: 'Обзор и тестирование экшн-камеры Xiaomi Yi (Sport): изящный ...',
    description: 'Экшн-камера Xiaomi Yi (Sport). Для чего производитель выходит на рынок? Чтобы ' +
    'что-то продать. А как он выходит на рынок? С рекламой и звонкими ...',
  },
  {
    index: 16,
    url: 'http://betanews.com/2016/05/05/xiaomi-yi-2-official-specs-price/',
    title: 'Xiaomi Yi 2 action camera now official - BetaNews',
    description: 'Xiaomi Yi 2 Xiaomi Yi is the most attractive action cameras for consumers ' +
    'on a budget. For just under $80, you get pretty much everything you could ...',
  },
  {
    index: 17,
    url: 'http://www.3dnews.ru/932793',
    title: 'Экшен-камера Xiaomi Yi 4K представлена официально - 3DNews',
    description: 'Компания Xiaomi, как и предполагалось, анонсировала устройство Yi 4K Action ' +
    'Camera 2 — компактную камеру нового поколения для спортсменов, ...',
  },
  {
    index: 18,
    url: 'http://www.amazon.com/Xiaoyi-Action-Camera-Wi-Fi-White/dp/B0148TNDXY',
    title: 'Amazon.com: Xiaoyi Yi Action Camera with Wi-Fi, White ...',
    description: 'Xiaomi Yi Action Camera Accessories Kit: Kupton Xiaoyi Waterproof Housing Case' +
    ' + ... Newmowa Waterproof Housing Case for XIAOMI YI Sports Camera, ...',
  },
  {
    index: 19,
    url: 'http://4pda.ru/2016/05/12/297113/',
    title: 'Xiaomi Yi 4K Action Camera 2 может снимать 1080p-видео с ...',
    description: 'Компания Xiaomi, как и ожидалось, официально представила второе поколение ' +
    'экшен-камеры Xiaomi Yi 4K Action Camera. Камера оснащена ...',
  },
  {
    index: 20,
    url: 'http://www.gizchina.com/2016/05/12/watch-xiaomi-yi-camera-2-first-hands-video/',
    title: 'Watch: Xiaomi Yi Camera 2 first hands on video - Gizchina.com',
    description: 'Watch here a basic overview and hands on with the new touch-screen operated,' +
    ' 4K compatible Xiaomi Yi Camera 2 action camera!',
  },
  {
    index: 21,
    url: 'https://dashcamtalk.com/xiaomi-yi-dash-cam/',
    title: 'Xiaomi Yi Dash Cam | Dash Cam Talk',
    description: 'The Xiaomi Yi Dash Cam is the first dash camera released by the Chinese ' +
    'electronics manufacturer Xiaomi Inc. This camera has been released approximately 1 ...',
  },
  {
    index: 22,
    url: 'http://www.banggood.com/Original-XiaoMi-Yi-Z23-Version-Ambarella-A7LS-BSI-CMOS-WIFI-' +
    'Sports-Action-Camera-p-995831.html',
    title: 'Original XiaoMi Yi Z23L Version Ambarella A7LS BSI CMOS WIFI ...',
    description: 'Only US$76.99, buy best Original XiaoMi Yi Z23L Version Ambarella A7LS BSI ' +
    'CMOS WIFI Sports Action Camera sale online store at wholesale price.US/EU ...',
  },
  {
    index: 23,
    url: 'http://fpvlab.com/forums/showthread.php?45118-Xiaomi-Yi-SuperView-Mod-(-more!)',
    title: 'Xiaomi Yi SuperView Mod (+more!) - FPVLab',
    description: 'The Xiaomi Yi camera is really great when it comes to value, but it lacks ' +
    'in quality of video. This is mostly not a hardware issue, it is a firmware issue. The ' +
    'Xiaomi ...',
  },
  {
    index: 24,
    url: 'http://www.trustedreviews.com/xiaomi-yi-car-wifi-dvr-review',
    title: 'Xiaomi Yi Car WiFi DVR review | TrustedReviews',
    description: 'The Xiaomi Yi is a temptingly cheap dashboard camera, but it has some notable' +
    ' flaws.',
  },
  {
    index: 25,
    url: 'http://vk.com/xiaomi_yi',
    title: 'Xiaomi | ВКонтакте',
    description: 'Здесь вы сможете не только приобрести проверенные товары от Xiaomi, но и ' +
    'моментально найти ... Ка кдумаете, квадрик h8mini потянет xiaomi yi?',
  },
  {
    index: 26,
    url: 'http://www.tawbaware.com/xiaomiyi.htm',
    title: 'Hacking the Xiaomi Yi Action Camera - TawbaWare',
    description: 'The Xiaomi Yi action camera was introduced in early 2015. It is similar in ' +
    'many respects to some of the popular GoPro cameras, but costs considerably less ...',
  },
  {
    index: 27,
    url: 'https://play.google.com/store/apps/details?id=com.xiaomi.xy.sportscamera&hl=en',
    title: 'YI Action - YI Action Camera - Android Apps on Google Play',
    description: 'Control, preview, and share instantly. Find more ways to play with your YI ' +
    'Action Camera with the YI Action app. Compatible with both Android and iOS, the app ...',
  },
  {
    index: 28,
    url: 'http://www.geekbuying.com/item/-HK-Stock---International-Edition--Xiaomi-Yi-Smart-Car-' +
    'DVR-1080P-60FPS-160-Degree-WiFi-Built-in-240mAh-Battery-Support-Android---IOS---Grey-' +
    '363571.html',
    title: 'Xiaomi Yi Smart Car DVR 1080P 160 WiFi 240mAh for Android & IOS',
    description: '64.99 Free Shipping, Wholesale Price, [HK Stock] [International Edition] ' +
    'Xiaomi Yi Smart Car DVR Dash Camera 1080P 60FPS 165 Degree WiFi Built-in ...',
  },
  {
    index: 29,
    url: 'http://www.xiaomi.ua/sport-camera/',
    title: 'Xiaomi Экшн камеры купить в Киеве, Харькове, Одессе | Экшн ...',
    description: 'Новинки Xiaomi Экшн камеры от интернет-магазина www.xiaomi.ua по ... Комплект' +
    ' Xiaomi Yi Sport Green Basic Edition + Waterproof box + Монопод. Цена:.',
  },
  {
    index: 30,
    url: 'http://xiaomi-mi.com/yi-action-camera/',
    title: 'Yi Action Camera - Xiaomi',
    description: '34 products ... Xiaomi Yi Action Camera Cycle Sports Accessories Kit · Buy now ' +
    '... Xiaomi Yi Action Camera Monopod Selfie Stick + Bluetooth Remote Control.',
  },
  {
    index: 31,
    url: 'https://www.facebook.com/YiCamera/',
    title: 'Xiaomi Yi Camera - Facebook',
    description: 'Xiaomi Yi Camera. 17925 likes · 366 talking about this. Group for people ' +
    'who use Xiaomi YiCamera and accessories!',
  },
  {
    index: 32,
    url: 'http://www.citrus.ua/shop/goods/action-cameras/2094/250715',
    title: 'Экшн-камера Xiaomi Yi Sport White Basic Edition, купить Экшн ...',
    description: 'В нашем интернет магазине вы можете купить Экшн-камера Xiaomi Yi Sport White ' +
    'Basic Edition по выгодной цене! Быстрая доставка • бонусы за покупку.',
  },
  {
    index: 33,
    url: 'http://www.techfunology.com/electronics/digital-cameras/action-cams/xiaomi-yi-review-' +
    '2k-wifi-action-camera/',
    title: 'Xiaomi Yi review: 2k wifi action camera | TechFunology.com',
    description: 'Xiaomi Yi review, 2k 30fps cheap action camera, with wifi, bluetooth. Cheap ' +
    'action cam with excellent video and photographic quality, excellent value for money.',
  },
  {
    index: 34,
    url: 'https://www.amazon.co.uk/Xiaomi-Sports-Action-Camera-120fps/dp/B00U6IHO2S',
    title: 'Xiaomi Yi Sports Action Camera 1080p 60fps 720p 120fps: Amazon ...',
    description: 'Original Xiaomi Yi Sports Camera Action Cam WiFi Bluetooth 4.0 A7LS Ambarella' +
    ' 1080P Waterproof Camera 16.0MP Xiaoyi Travel My Action Sport Camera ...',
  },
  {
    index: 35,
    url: 'https://market.yandex.ru/product--xiaomi-yi-action-camera-travel-edition/12408262',
    title: 'Xiaomi YI Action Camera Travel Edition — Видеокамеры — купить ...',
    description: 'Подробные характеристики Xiaomi YI Action Camera Travel Edition, отзывы ' +
    '' +
    'покупателей, обзоры и обсуждение товара на форуме. Выбирайте из более ...',
  },
  {
    index: 36,
    url: 'https://www.instagram.com/xiaomiyi_id/',
    title: 'XIAOMI YI INDONESIA (@xiaomiyi_id) • Instagram photos and videos',
    description: 'XIAOMI YI INDONESIA. SEE DIFFERENT . LINE : @xiaomiyi_id goo.gl/JXJYJc. 1' +
    ',203 posts; 47.6k followers; 5 following . YiShoot by @bramsty - Nobody said it ...',
  },
  {
    index: 37,
    url: 'http://www.peauproductions.com/collections/xiaomi-yi-camera-lenses',
    title: 'Xiaomi Yi Lenses – Peau Productions',
    description: 'Items 1 - 10 of 10 ... 4.00mm f/1.6 90d HFOV 3MP. $40.00 USD. $80.00 USD. ' +
    'SALE. 4.14mm f/3.0 74d HFOV 5MP (No Distortion). $42.50 USD. $85.00 USD. SALE.',
  },
  {
    index: 38,
    url: 'https://github.com/deltaflyer4747/Xiaomi_Yi',
    title: 'GitHub - deltaflyer4747/Xiaomi_Yi: Xiaomi Yi Camera settings via ...',
    description: 'Xiaomi Yi Camera settings via python (PC) script. Contribute to Xiaomi_Yi ' +
    'development by creating an account on GitHub.',
  },
  {
    index: 39,
    url: 'http://www.quadcopterflyers.com/2015/06/Xiaomi-yi-camera-bluetooth-feature-capture' +
    '-images.html',
    title: 'How to Use Bluetooth With Xiaomi Yi Camera | Quadcopter | Drone ...',
    description: 'guide on how to use the Bluetooth feature of Xiaomi Yi Camera to capture ' +
    'images from the selfie-stick and monopods, Xiaomi Lauched Bluetooth Remote ...',
  },
  {
    index: 40,
    url: 'https://www.jakartanotebook.com/search?key=xiaomi+yi',
    title: 'Pencarian produk xiaomi yi | JakartaNotebook.com',
    description: 'Hasil pencarian xiaomi yi. xiaomi yi yang termurah dan terlengkap hanya ' +
    'ada di JakartaNotebook.com.',
  },
  {
    index: 41,
    url: 'https://oscarliang.com/xiaomi-yi-camera-60fps-fpv/',
    title: 'Xiaomi Yi Action Camera Review - 60FPS For FPV - Oscar Liang',
    description: 'Xiaomi Yi Action Camera is probably the cheapest HD camera, that is capable ' +
    'of recording 1080p footage at 60fps. Also the quality is not bad at all, and it\'s ...',
  },
  {
    index: 42,
    url: 'http://www.honorbuy.it/51-xiaomi-yi-sports-camera',
    title: 'Xiaomi Yi Sports Camera - HonorBuy Italia',
    description: 'Piccola & Potente Dispone di un sensore Sony Exmor R con retroilluminazione ' +
    '( BSI) in grado di registrare video full HD (1080p) a 60 FPS. Acquistare Xiaomi Yi ...',
  },
  {
    index: 43,
    url: 'http://en.miui.com/thread-193380-1-1.html',
    title: 'Change timezone on Xiaomi Yi Home Smart Camera - Mi Gadgets ...',
    description: 'Edit the file and addTimezone setting is weird:GMT-1 is GMT+9 Japan.GMT+1 ' +
    'is GMT+7 Viet ... Change timezone on Xiaomi Yi Home Smart Camera ,Xiaomi ...',
  },
  {
    index: 44,
    url: 'http://www.androidheadlines.com/2016/05/xiaomi-yi-4k-action-camera-launches-china-200.' +
    'html',
    title: 'Xiaomi Yi 4K Action Camera Launches In China For $200 ...',
    description: 'Xiaomi entered the action camera market last year with the release of the ' +
    'Xiaomi Yi, which took consumers by surprise thanks to its low price of $80. Excit.',
  },
  {
    index: 45,
    url: 'http://www.dronebench.com/xiaomi-yi-review-the-camera-which-frighten-the-gopro/',
    title: 'Xiaomi YI review, the camera which frighten the GoPro - Drone bench',
    description: 'Xiaomi YI review, the camera which frighten the GoPro. Review. May 20, 2015.' +
    ' 0 3193. No, no I do not exagerate … I do confess that my heading is a little bit ...',
  },
  {
    index: 46,
    url: 'http://www.lesnumeriques.com/action-cam/xiaomi-yi-camera-p28453/test.html',
    title: 'Xiaomi Yi Camera : Test complet - Les Numériques',
    description: 'La Xiaomi Yi est un petit produit simple et de bonne qualité. Comme sur ' +
    'la plupart des concurrentes, ses principaux défauts sont sa faible autonomie et la ...',
  },
  {
    index: 47,
    url: 'https://www.thingiverse.com/tag:xiaomi',
    title: 'Things tagged with \'Xiaomi\' - Thingiverse',
    description: '(119 Things). Martian GoPro / XiaoMi 30° Mount by SnappyHH 1 day ago. 4 7 2 ..' +
    '. Xiaomi YI - Mount for Tarot Gimbal T2D - V2 by benzed78 Apr 30, 2016. 0 1 0.',
  },
  {
    index: 48,
    url: 'http://xistore.by/xiaomi-yi-action-camera.html',
    title: 'Камера Xiaomi Yi Action - Xistore.by',
    description: 'Xiaomi Yi Smart Camera можно разместить в любом уголке дома и через мобильный' +
    ' телефон или планшет в любое время наблюдать за тем, что ...',
  },
  {
    index: 49,
    url: 'https://bestactioncamera.net/gitup-git2-vs-xiaomi-yi/',
    title: 'Gitup Git2 Vs Xiaomi Yi - Best Action Camera',
    description: 'Today we are going to compare the fantastic Gitup Git2 and the Xiaomi Yi action' +
    ' cameras. Gitup Git2 is a fantastic action camera that we recently reviewed and ...',
  },
  {
    index: 50,
    url: 'http://www.aliexpress.com/store/product/Pre-Sale-Original-Xiaomi-Yi-Camera-Waterproof' +
    '-Case-Mi-Yi-40M-Diving-Sports-Waterproof-Box-Yi/1041024_32299341819.html',
    title: 'Aliexpress.com : Buy New Version IN STOCK! Original Xiaomi Yi ...',
    description: 'Find More Camera/Video Bags Information about New Version IN STOCK! Original ' +
    'Xiaomi Yi Camera Waterproof Case, Mi Yi 40M Diving Sports Waterproof Box ...',
  },
  {
    index: 51,
    url: 'http://www.techmoan.com/blog/2016/2/17/xiaomi-yi-xiaoyi-dash-cam-review.html',
    title: 'Techmoan - Techmoan - Xiaomi Yi (Xiaoyi) Dash-Cam Review',
    description: 'Over the last year, Xiaomi\'s Yi action camera has turned out to be a very ' +
    'popular model due to its keen price and excellent image quality. So when the company ...',
  },
  {
    index: 52,
    url: 'http://androidcommunity.com/xiaomi-yi-home-camera-now-available-in-the-us-through-' +
    'amazon-20151221/',
    title: 'Xiaomi Yi Home Camera now available in the US through Amazon ...',
    description: 'Xiaomi has quickly made a follow-up to the Yi Action Camera introduced early ' +
    'this month in the form of the Yi Home Camera. The name itself tells us the device.',
  },
  {
    index: 53,
    url: 'http://www.xiaomimalaysia.net/xiaomi-yi-camera/',
    title: 'Xiaomi Yi Action Camera | Xiaomi Malaysia',
    description: 'Yes, that is right, be surprised because Xiaomi is not just manufacturing ' +
    'smartphones but they have now come up with the all new Xiaomi Yi Action Camera.',
  },
  {
    index: 54,
    url: 'http://xiaoyi.querex.be/',
    title: 'Xiaomi YI ants smart camera - Unofficial info page - Firmwares',
    description: 'Xiaomi Xiaoyi Ants unofficial info page. Stream urls. Replace 192.168.1.10' +
    ' with your Xiaoyi Ants ip address. To enable RTSP you need to flash a firmware with ...',
  },
  {
    index: 55,
    url: 'http://great3d.com/xiaomi-yi-tilted-camera-mount-made-from-tpu-limited-lifetime-' +
    'warranty-strap-and-camera-not-included/',
    title: 'Xiaomi Yi tilted camera mount made from TPU (limited lifetime ...',
    description: 'This is the TPU Xiaomi Yi mount for the MRM Mini 250 and ZMR Frames. Zero- ' +
    'Jello! Case covers SD card, so no more unwanted ejections. Now available for ...',
  },
  {
    index: 56,
    url: 'http://www.shapeways.com/product/PDF3NTU58/brahma-6-mark-two-for-xiaomi-yi-360-videos',
    title: 'Brahma-6 Mark Two For Xiaomi Yi 360 videos (PDF3NTU58) by ...',
    description: 'Brahma-6 Mark Two For Xiaomi Yi 360 videos (PDF3NTU58) by clusteruk on ' +
    'Shapeways. Learn more before you buy, or discover other cool products in ...',
  },
  {
    index: 57,
    url: 'http://nutseynuts.blogspot.com/2015/06/xiaomi-yi-action-cam-custom-scripts.html',
    title: 'nutseynuts: Xiaomi Yi action cam custom scripts.',
    description: 'Hi there! I decided to put here all the info I got about hacking Xiaomi Yi ' +
    'cam using autoexec.ash script, telnet-access, customizing firmware and etc. Check my ...',
  },
  {
    index: 58,
    url: 'http://www.vrcolony.net/tutorialsblog/2015/7/31/why-i-chose-the-xiaomi-yi',
    title: 'Why Xiaomi Yi? — VRColony.net',
    description: 'Research and comparison led me to the Xiaomi Yi, a $75 camera with specs ' +
    'that rival the $400 GoPro. It\'s got nearly everything I want in a camera for VR, ...',
  },
  {
    index: 59,
    url: 'http://www.instructables.com/id/Xiaomi-Yi-Lens-Cap/',
    title: 'Xiaomi Yi Lens Cap - Instructables',
    description: 'Quick back story on the camera if you don\'t have one but are interested.' +
    ' They\'re a great cheap alternative to a gopro or standard action camera. They use a ...',
  },
  {
    index: 60,
    url: 'http://www.everbuying.net/product1119278.html',
    title: 'Original Xiaomi Yi 1080P Car DVR - Free Shipping | Everbuying',
    description: 'Everbuying offers high quality Original Xiaomi Yi 1080P FHD 60fps Car WiFi' +
    ' DVR 2.7 inches Screen ADAS Dash Cam CN Version (GRAY) at wholesale price ...',
  },
  {
    index: 61,
    url: 'http://galaktyczny.pl/2015/07/01/xiaomi-yi-recenzja-test-opinia/',
    title: 'Xiaomi Yi - recenzja świetnej i taniej kamery sportowej',
    description: 'Jak w naszym teście poradziła sobie kamera Xiaomi Yi? Tak jeszcze wstępnie' +
    ' pisząc, Xiaomi to bardzo młoda i zarazem zaskakująca firma. Została założona ...',
  },
  {
    index: 62,
    url: 'http://www.gsmarena.com/xiaomi_yi_action_camera_lands_in_us_for_100-blog-15356.php',
    title: 'Xiaomi YI Action Camera lands in US for $100 - GSMArena blog',
    description: 'Xiaomi first launched the YI Action Camera in China back in March this year.' +
    ' Now , nine months later, the camera has officially arrived in the United...',
  },
  {
    index: 63,
    url: 'http://bokeh.digitalrev.com/article/xiaomi-yi-2-4k-action-cam-specs-leaked',
    title: 'Xiaomi Yi 2 4K Action Cam Specs Leaked - Bokeh by DigitalRev',
    description: 'Following the leak of several promotional stills, some juicy details about ' +
    'the Xiaomi Yi Action Camera 2\'s specs have come to light.',
  },
  {
    index: 64,
    url: 'http://dashboardcamerareviews.com/xiaomi-yi-xiaoyi-dash-cam/',
    title: 'Xiaomi Yi Dash Cam - Dashboard Camera Reviews',
    description: 'Xiaomi Yi Dash Cam Review. The Xiaomi Yi (short version: Xiaoyi) Dash Cam ' +
    'is a specialized version of the Xiaomi Yi Action Cam. Don\'t confuse the two, as the ...',
  },
  {
    index: 65,
    url: 'http://www.xataka.com/analisis/xiaomi-yi-camera-analisis',
    title: 'Xiaomi Yi Camera, análisis: barata y fácil para tomar fotos, no apta ...',
    description: 'Review en vídeo de la Xiaomi Yi Camera. Arrancamos nuestro análisis de ' +
    'esta cámara de acción asequible con su correspondiente vídeo para conocerla con ...',
  },
  {
    index: 66,
    url: 'http://co-op.kinja.com/your-favorite-budget-action-cam-is-the-xiaomi-yi-1759461934',
    title: 'Your Pick For Best Budget Action Cam: Yi - Kinja Co-Op',
    description: 'We didn\'t expect a ton of nominations in our budget action cam Co-Op—GoPro ' +
    'still dominates the market, despite their financial troubles—but one particular ...',
  },
  {
    index: 67,
    url: 'http://www.honorbuy.com/263-head-strap-mount-quick-clip-for-yi-sports-camera.html',
    title: 'Head Strap Mount + Quick Clip for Xiaomi Yi Sports ... - HonorBuy.com',
    description: 'Buy Xiaomi Yi Sports Camera Accessories on honorbuy.com , Fully adjustable ' +
    'to fit all sizes can be used to attach your Yi Sports Camera to a backwards ...',
  },
  {
    index: 68,
    url: 'http://www.cnx-software.com/2016/05/09/xiaomi-yi-4k-action-camera-2-offers-an-alternat' +
    'ive-to-gopro-hero4-black-camera/',
    title: 'Xiaomi Yi 4K Action Camera 2 Offers an Alternative to GoPro ...',
    description: 'Xiaomi is about to launch a new 4K sports camera with Xiaomi Yi 4K powered by ' +
    'Ambarella A9SE dual core Cortex A9 processor and supporting video ...',
  },
  {
    index: 69,
    url: 'https://www.reddit.com/r/Xiaomi/comments/37vpyr/do_gopro_accessories_work_with_the_' +
    'xiaomi_yi/',
    title: 'Do GoPro accessories work with the Xiaomi Yi? : Xiaomi - Reddit',
    description: 'I have a bunch of accessories lying around for my GoPro and would be very ' +
    'happy if the Xiaomi is compatible with them.',
  },
  {
    index: 70,
    url: 'http://keddr.com/2015/07/xiaomi-yi-action-camera-sam-sebe-operator/',
    title: 'Xiaomi Yi Action Camera – сам себе оператор | Keddr.com',
    description: 'Xiaomi Yi я честно отжала у Саши Ляпоты ради путешествия в Исландию. ' +
    'Особенных экстремальных вылазок не ожидалось, но посмотреть рыбьим ...',
  },
  {
    index: 71,
    url: 'http://hispeedcams.com/xiaomi-yi-action-camera-has-a-slowmo-feature/',
    title: 'Xiaomi YI Action Camera Has a Slowmo Feature! - Hi Speed Cameras',
    description: 'The Xiaomi Yi Action Camera formerly only marketed in China is now reaching ' +
    'the US via Amazon with Prime delivery. The camera is an ultra cheap clone of ...',
  },
  {
    index: 72,
    url: 'http://imgur.com/gallery/1KyNj',
    title: 'Sea water leaked inside my Xiaomi Yi :( - Album on Imgur',
    description: 'Sea water leaked inside my Xiaomi \'Official\' waterproof case after first use' +
    ' ! Thankfully the repair went well and the cam is now working just fine. 58 points.',
  },
  {
    index: 73,
    url: 'http://www.tinydeal.com/xiaomi-yi-ambarella-a7ls-bluetooth-wi-fi-16mp-fhd-action-' +
    'camera-p-147790.html',
    title: 'Xiaomi Yi Ambarella A7LS Bluetooth Wi-Fi 16MP FHD Action ... - Like',
    description: 'Click to buy Xiaomi Yi Ambarella A7LS Bluetooth Wi-Fi 16MP FHD Action Camera ' +
    'ECM-386722 from TinyDeal.com with wholesale price and free shipping!',
  },
  {
    index: 74,
    url: 'http://allegro.pl/xiaomi-yi-kamera-sportowa-obudowa-8x-akcesoria-i5478001056.html',
    title: 'XIAOMI YI KAMERA SPORTOWA + OBUDOWA + 8X AKCESORIA ...',
    description: 'Kup teraz na allegro.pl za 429,00 zł - XIAOMI YI KAMERA SPORTOWA + OBUDOWA +' +
    ' 8X AKCESORIA (5478001056). Allegro.pl - Radość zakupów i ...',
  },
  {
    index: 75,
    url: 'http://thegadgetflow.com/portfolio/xiaomi-yi-sports-camera/',
    title: 'XIAOMI Yi Sports Camera » Review - Gadget Flow',
    description: 'If you\'ve been eyeing one of those affordable action cameras but don\'t want ' +
    'to spend major cash on something that might break after a few outings, then you.',
  },
  {
    index: 76,
    url: 'http://cameras.mercadolivre.com.br/cameras-e-accesorios/xiaomi-yi',
    title: 'Xiaomi Yi - Câmeras e Acessórios no Mercado Livre Brasil',
    description: 'Os melhores preços em Xiaomi Yi - Câmeras e Acessórios estão no Mercado Livre ' +
    'Brasil. Todas as marcas e modelos em Filmadoras, Acessórios para ...',
  },
  {
    index: 77,
    url: 'http://hi-tech.ua/article/obzor-ekshen-kameryi-xiaomi-yi-kogda-voprosov-ne-ostaetsya/',
    title: 'Обзор экшн-камеры Xiaomi Yi: когда вопросов не остается | hi ...',
    description: 'Может ли Xiaomi Yi, будучи в разы дешевле конкурентов, соревноваться с ними ' +
    'по качеству и функциональности – выясним в тесте.',
  },
  {
    index: 78,
    url: 'http://indianexpress.com/article/technology/gadgets/xiaomi-yi-4k-action-camera-launch-' +
    'specs-price-2800470/',
    title: 'Xiaomi\'s new YI 4K Action Camera 2 goes on sale in China | The ...',
    description: 'Xiaomi has updated its YI action with 4K video recording feature. The new YI ' +
    '4K Action Camera 2 is capable of shooting 4K videos at 30fps, full HD videos at ...',
  },
  {
    index: 79,
    url: 'https://geektimes.ru/company/cplaza/blog/259994/',
    title: 'Страшный сон GoPro: обзор action-камеры Xiaomi Yi / Блог ...',
    description: 'А в начале 2015 года компания начала развивать новую для себя нишу action ' +
    '-камер, выпустив Xiaomi Yi. После этого журналисты технический изданий ...',
  },
  {
    index: 80,
    url: 'http://nis-store.com/action-cameras/xiaomi-yi-action-camera-black/',
    title: 'Xiaomi Yi Action Camera Black - NIS-Store',
    description: '$14. Xiaomi Yi Action Camera Silicone Protective Case Black ... Xiaomi Mi ' +
    'Power Bank 10000mAh Silver ... Xiaomi Mi Band Pulse Black + Mi Band Strap Green.',
  },
  {
    index: 81,
    url: 'http://www.giztop.com/xiaomi-yi-action-camera-2.html',
    title: 'Buy Xiaomi Yi Action Camera 2 - Free Shipping, no customs tax',
    description: 'Xiaomi Yi Action Camera 2 trumps the specs of GoPro Hero4 with 4K, 30fps ' +
    'video and up to 128GB of memory at about half the price, it come with extra ...',
  },
  {
    index: 82,
    url: 'http://360cameraonline.com/xiaomi-yi-action-sport-camera-3d-360-rig/',
    title: 'Xiaomi Yi Action Sport camera 3D 360 rig | 360 Camera Online',
    description: 'We all know that getting a bunch of GoPro\'s is very costly. His rig is ' +
    'based on Xiaomi Yi Cameras and seems to be working quite well. Take a look at the footage ...',
  },
  {
    index: 83,
    url: 'http://www.lazada.com.my/original-xiaomi-yi-xiaoyi-action-sport-camera-uv-filter-' +
    'lens-protector-2558049.html',
    title: 'Original Xiaomi Yi XiaoYi Action Sport Camera UV Filter Lens ...',
    description: 'Buy Original Xiaomi Yi XiaoYi Action Sport Camera UV Filter Lens Protector ' +
    'online at Lazada. Discount prices and promotional sale on all. Free Shipping.',
  },
  {
    index: 84,
    url: 'http://uae.souq.com/ae-en/xiaomi-yi-1080p-full-hd-flash-memory-action-sports-camera-' +
    'wifi-28-6x-optical-zoom-white-8085088/i/',
    title: 'Xiaomi Yi 1080p Full HD Flash Memory Action Sports Camera - WiFi ...',
    description: 'The XiaoMi Yi 16MP Sports Action HD Camera with WiFi (Basic Version) is a ' +
    'standalone imaging device that can be carried with you on all your crazy, ...',
  },
  {
    index: 85,
    url: 'http://www.phantompilots.com/threads/xiaomi-yi-wifi-or-sjcam-sj4000-wifi.42631/',
    title: 'Xiaomi Yi wifi or SJCAM Sj4000 wifi | DJI Phantom Drone Forum ...',
    description: 'I\'ve been looking on youtube and various sites on action cameras and now ' +
    'my choice stands between the Xiaomi Yi 16MP wifi and the SJCam SJ4000 wifi.',
  },
  {
    index: 86,
    url: 'http://www.oppomart.com/xiaomi-yi-4k-action-camera-2.html/',
    title: 'Buy Xiaomi Yi 4K Action Camera 2 - Oppomart',
    description: 'Xiaomi Yi 4K Action Camera 2 trumps the specs of GoPro Hero4 with 4K video ' +
    'and up to 128GB of memory at about half the price, it come with extra accessories ...',
  },
  {
    index: 87,
    url: 'http://www.techtimes.com/articles/113457/20151205/xiaomi-launches-its-first-product-' +
    'in-the-us-sub-100-yi-action-camera-available-now-on-amazon.htm',
    title: 'Xiaomi Launches Its First Product In The US: Sub-$100 Yi Action ...',
    description: 'The Yi action camera, Xiaomi\'s first gadget to hit retail in the U.S, just ' +
    'went on sale. The camera costs less than $100 and is now available on Amazon.',
  },
  {
    index: 88,
    url: 'https://ru-mi.com/device/camera/',
    title: 'Камера Xiaomi Yi – лучшее решение по доступной цене!',
    description: 'В интернет-магазине ру-ми.ком вы можете купить Xiaomi Yi Action Camera и ' +
    'необходимые для неё аксессуары: монопад, аквабокс, крепления на шлем, ...',
  },
  {
    index: 89,
    url: 'http://www.hobbyking.com/hobbyking/store/__91046__Tarot_T_2D_V2_Xiaomi_Yi_Sports_' +
    'Camera_Brushless_Camera_Gimbal_and_ZYX22_Controller.html',
    title: 'Tarot T-2D V2 Xiaomi Yi Sports Camera Brushless Camera Gimbal ...',
    description: 'Tarot needs no introduction, their name is synonymous with quality and their ' +
    '2 axis ZYX22 gimbal controller and T-2D v2 brushless gimbal for the Xiaomi Yi ...',
  },
  {
    index: 90,
    url: 'http://www.slashgear.com/xiaomis-yi-home-camera-makes-its-way-to-amazon-21419381/',
    title: 'Xiaomi\'s Yi Home Camera makes its way to Amazon - SlashGear',
    description: 'Just two weeks after it made available its own version of a budget-friendly ' +
    'action camera, Xiaomi is launching another one of its products in the US. And no,',
  },
  {
    index: 91,
    url: 'https://www.tokopedia.com/hot/xiaomi-yi',
    title: 'Jual Xiaomi Yi | Tokopedia',
    description: 'Jual Xiaomi Yi, harga terbaik dan lengkap dari ribuan toko online se-' +
    'Indonesia. Transaksi di Tokopedia aman, pakai rekening bersama.',
  },
  {
    index: 92,
    url: 'http://www.eeekit.com/blog/xiaomi-yi-action-camera-review/',
    title: 'Xiaomi Yi Action Camera Review - EEEKit',
    description: 'Introduction and review of Xiaomi Yi Action Camera, an entry-level budget-' +
    ' friendly action cam. Comparing Xiaomi Yi vs GoPro HERO vs Polaroid Cube.',
  },
  {
    index: 93,
    url: 'http://tech.firstpost.com/news-analysis/xiaomi-yi-4k-action-camera-with-12mp-sony-' +
    'sensor-announced-in-china-314858.html',
    title: 'Xiaomi Yi 4K action camera with 12MP Sony sensor announced in ...',
    description: 'Xiaomi has announced the upgrade to its Yi Action Camera 2 – called the Yi' +
    ' 4K. As the name suggests, it can shoot 4K videos at 30fps, along with full HD ...',
  },
  {
    index: 94,
    url: 'http://pingvin.pro/gadgets/reviews-gadgets/xiaomi-yi-oglyad-b-yuti-kamery.html',
    title: 'Xiaomi YI - огляд б\'юті-камери - Pingvin.Pro',
    description: 'Xiaomi Yi 1. Екшн-камери… Екшн-камери… Екшн. Ви знаєте що таке екшн? Так… ' +
    'останнім часом екшн камери здебільшого використовують саме для ...',
  },
  {
    index: 95,
    url: 'http://en.jd.com/283446.html',
    title: 'Original Xiaomi Yi cam Sports Camera WIFI 16MP 1080P Action ...',
    description: 'JD Global shopping mall sells Original Xiaomi Yi cam Sports Camera WIFI ' +
    '16MP 1080P Action Camera Bluetooth 4.0 for customers who need gopro, sports ...',
  },
  {
    index: 96,
    url: 'http://forum.mapillary.io/t/xiaomi-yi-action-cam/131',
    title: 'Xiaomi Yi action cam - Mapillary Community Forum',
    description: 'Many people have the Xiaomi Yi and because it provides a nice image ' +
    'quality and is fairly cheap I have thought of getting 1 or 2 for alternative angles.',
  },
  {
    index: 97,
    url: 'http://www.androidpolice.com/2015/12/03/xiaomis-yi-action-camera-now-available-in-' +
    'the-us-for-99-95-via-amazon/',
    title: 'Xiaomi\'s YI Action Camera Now Available In The US For $99.95 Via ...',
    description: 'GoPro is by far the most recognizable action camera brand, but Yi (a Xiaomi' +
    ' company) has been selling the YI Action Camera internationally for some time. Now ...',
  },
  {
    index: 98,
    url: 'http://www.fonearena.com/blog/184740/xiaomi-yi-4k-action-camera-with-12mp-sony-sensor-' +
    'announced.html',
    title: 'Xiaomi YI 4K action camera with 12MP Sony sensor announced',
    description: 'Xiaomi\'s YI has introduced a new YI action camera with support for 4K video ' +
    'recording at 30 fps, including hyperlapse and has a 12MP Sony IMX377 sensor.',
  },
  {
    index: 99,
    url: 'https://www.flickr.com/photos/nsutrich/albums/72157654522911101/',
    title: 'Xiaomi Yi Action Camera Review | Flickr - Photo Sharing!',
    description: 'Xiaomi Yi Action Camera Review. Photo mode samples. Show more. 14 photos · ' +
    '1,371 views. Gwanatu By: Gwanatu · Xiaomi-Yi-01 by Gwanatu. Xiaomi-Yi-02 ...',
  },
  {
    index: 100,
    url: 'http://thetechnews.com/2016/05/05/the-most-awaited-xiaomi-yi-4k-action-camera-is-here/',
    title: 'The Most Awaited Xiaomi Yi 4K Action Camera is Here ...',
    description: 'For those who have been waiting for a long time and kept their faith on Xiomi,' +
    ' finally, the wait is over with the new release of the Xiaomi Yi 4K Action Camera ...',
  },
  {
    index: 101,
    url: 'http://gadgets.ndtv.com/cameras/news/xiaomi-yi-action-camera-2-with-4k-video-support' +
    '-launched-837250',
    title: 'Xiaomi Yi Action Camera 2 With 4K Video Support Launched ...',
    description: 'Xiaomi initially tried its hands at action cameras, a market traditionally ' +
    'dominated by GoPro, last year with the launch of Yi Action Camera priced at dirt cheap ...',
  },
  {
    index: 102,
    url: 'https://vimeo.com/156252934',
    title: 'Xiaomi Yi Car DVR Test Footage on Vimeo',
    description: '',
  },
  {
    index: 103,
    url: 'http://www.pcmech.com/article/xiaomi-yi-and-gopro-hero-4-battle-of-the-action-cameras/',
    title: 'Xiaomi Yi and GoPro Hero 4 - Battle of the Action Cameras - PCMech',
    description: 'In response, China’s tech giant, Xiaomi, released its own action camera ' +
    'called Xiaomi Yi. ... The GoPro Hero 4 Silver is way more expensive than the Xiaomi Yi, ' +
    'so it is rational to expect that it has better video quality. ... Bottom Line: The GoPro' +
    ' Hero 4 Silver is better than the Xiaomi ...',
  },
  {
    index: 104,
    url: 'http://andro-news.com/news/xiaomi-yi-4k-action-camera-2-ekshn-kamera-predstavlena.' +
    '-video-v-.html',
    title: 'Xiaomi Yi 4K Action Camera 2 – экшн-камера представлена ...',
    description: 'Xiaomi – не только сильный бренд в нише смартфонов, но и у него получается ' +
    'создавать приличного уровня экшн-камеры. У этих продуктов есть свои ...',
  },
  {
    index: 105,
    url: 'http://rozetka.com.ua/xiaomi_yi_car_dvr_gr/p7656526/',
    title: 'Rozetka.ua | Xiaomi Yi Car DVR 1080P WiFi Gray. Цена, купить ...',
    description: 'Xiaomi Yi Car DVR 1080P WiFi Gray в интернет-магазине Rozetka.ua. Тел: ' +
    '( 044) 537-02-22, 0 800 503-808. Доставка, гарантия, лучшие цены!',
  },
  {
    index: 106,
    url: 'http://www.ibtimes.co.in/xiaomi-yi-4k-action-launched-china-features-price-other-' +
    'details-678793',
    title: 'Xiaomi Yi 4K Action launched in China: Features, price, other details',
    description: 'Xiaomi has launched a new camera called Xiaomi Yi 4K Action in China. It' +
    ' comes more than a year after the Chinese technology giant surprised gadget lovers ...',
  },
  {
    index: 107,
    url: 'http://www.bhphotovideo.com/c/compare/HTC_RE_Camera_Navy_vs_Xiaomi_Yi_Sport_Camera_G' +
    'reen/BHitems/1086897-REG_1170323-REG',
    title: 'Compare HTC RE Camera Navy vs Xiaomi Yi Sport Camera Green',
    description: 'Compare HTC RE Camera Navy vs Xiaomi Yi Sport Camera Green.',
  },
  {
    index: 108,
    url: 'http://sigandrone.com/xiaomi-yi-tpu-casewedge',
    title: 'SiganDrone - Xiaomi Yi Flexible Case/Wedge',
    description: 'Made from virtually indestructable TPU material. Perfect snug fit for Xiaomi ' +
    'Yi action cam. SD card and USB ports are still accessible without removing the ...',
  },
  {
    index: 109,
    url: 'http://metropolitanmonkey.com/xiaomi-yi-vs-sjcam-m10-plus-battle-2/',
    title: 'XIAOMI Yi vs. SJCAM M10+ - Battle #2 | Metropolitan Monkey',
    description: 'Bei den Wünschen zu Gegenüberstellungen wurde das Battle Xiaomi Yi vs. ' +
    'SJCAM M10+ genannt. Habt Ihr einen Favoriten? Dann drückt ihm die Daumen und ...',
  },
  {
    index: 110,
    url: 'http://www.dpreview.com/articles/6947522445/chinese-maker-xiaomi-challenges-gopro-' +
    'with-new-yi-action-camera',
    title: 'Chinese maker Xiaomi challenges GoPro with new Yi Action Camera',
    description: 'Updated 12/3/15: Xiaomi\'s Yi Action Camera, initially available only in ' +
    'China, is now being offered in the US through Amazon. Thanks to promising specs and a ...',
  },
  {
    index: 111,
    url: 'http://techpp.com/2016/05/13/xiaomi-yi-4k-action-camera-price/',
    title: 'Xiaomi Yi 4K Action Camera Announced for $183; Comes with ...',
    description: 'Yi cam is Xiaomi\'s take on GoPro action camera, and its latest iteration ' +
    'has got some neat features. The new Yi cam has been upgraded to take 4K videos at.',
  },
  {
    index: 112,
    url: 'http://digicamcontrol.com/phpbb/viewtopic.php?t=1264',
    title: 'Control of multiple Xiaomi Yi cameras - digiCamControl',
    description: 'We were amazed by DigiCamControl software and because of that we thought ' +
    'of choosing Xiaomi Yi cameras. But if it is possible to control so many of them is ...',
  },
  {
    index: 113,
    url: 'https://gopro-shop.by/useful-tips-for-working-with-gopro/xiaomi-yi-sbros-wi-fi-parolja-' +
    'i-nastroek/',
    title: 'Xiaomi Yi сброс Wi-Fi пароля и настроек - GoPro-Shop.by',
    description: 'Забыли Wi-Fi пароль от своей Xiaomi Yi? Или ваша экшн-камера Xiaomi Yi не ' +
    'работает и вы хотите сделать сброс настроек? - Так мы покажем и ...',
  },
  {
    index: 114,
    url: 'http://www.mudah.my/Xiaomi+Yi+Action+Camera-41733411.htm',
    title: 'Xiaomi Yi Action Camera - Cameras & Photography for sale in Bangi ...',
    description: 'New Cameras & LensesOther Brand/ Format Cameras & Lenses for sale for RM ' +
    '480 at Bangi, Selangor.',
  },
  {
    index: 115,
    url: 'http://www.teknopata.eus/wp-content/uploads/2015/07/yi-sport-camera-EN.pdf',
    title: 'Getting started with Yi Action Camera Included: Operating of a ...',
    description: 'Product Name: Yi Camera. In: 1А 5V. Standard consumption of energy: 2 w. ' +
    'Model:YDXJ01XY. The operating standard: GB/T 29298-2012. XIAOMI-MI.com.',
  },
  {
    index: 116,
    url: 'http://digital-cameras.specout.com/compare/2140-2332/Xiaomi-Yi-vs-Eken-H3R',
    title: 'Xiaomi Yi vs Eken H3R - Cameras Comparison',
    description: 'Compare Cameras: Xiaomi Yi vs Eken H3R. Compare detailed tech specs, f' +
    'eatures, expert reviews, and user ratings of these two cameras side by side.',
  },
  {
    index: 117,
    url: 'https://www.amazon.es/Xiaomi-Yi-Action-Videoc%C3%A1mara-deportiva/dp/B00UFC48B0',
    title: 'Xiaomi Yi Action - Videocámara deportiva Full HD 1080p, amarillo ...',
    description: 'Marca, Desconocido. Modelo, Yi Action. Peso del producto, 73 g. Dimensiones d' +
    'el producto, 4,2 x 2,1 x 6 cm. Número de modelo del producto, Yi Action.',
  },
  {
    index: 118,
    url: 'http://www.multiwiicopter.com/products/xiaomi-yi-action-fpv-hd-camera-australia-nz',
    title: 'Action Cam Xiaomi Yi - 1080p HD - White - original OEM AU ...',
    description: 'This is the genuine high-end Chinese mainland Xiaomi Yi 1080p action camera' +
    ' in stock in Australia /NZ (beware of fakes with soft focus)',
  },
  {
    index: 119,
    url: 'http://china-review.com.ua/news/7317-predstavlena-ekshn-kamera-xiaomi-yi-4k-action-' +
    'camera.html',
    title: 'Представлена экшн-камера Xiaomi Yi 4K Action Camera » China ...',
    description: 'Официально представлена новая экшн-камера Xiaomi Yi 4K Action Camera. Ее ' +
    'характеристики были известны заранее, поэтому интрига оставалось ...',
  },
  {
    index: 120,
    url: 'https://www.anekdote.co/volumes/new-perspectives/products/xiaomi-yi-camera',
    title: 'Xiaomi YI Camera | Anekdote',
    description: 'The Xiaomi Yi - a compact action camera with a powerful CMOS sensor. High ' +
    'quality footage in sunlight, cloudy weather, indoors and even at night.',
  },
  {
    index: 121,
    url: 'http://www.skroutz.gr/s/6598337/Xiaomi-Yi-Basic-Edition.html',
    title: 'Xiaomi Yi Basic Edition | Skroutz.gr',
    description: 'Βρες τιμές καταστημάτων για το Xiaomi Yi Basic Edition. Διάβασε απόψεις ' +
    'χρηστών και τεχνικά χαρακτηριστικά για το Xiaomi Yi Basic Edition ή ρώτησε την ...',
  },
  {
    index: 122,
    url: 'https://www.kogan.com/au/buy/xiaomi-yi-action-camera-green/',
    title: 'Xiaomi Yi Action Camera (Green) - Kogan.com',
    description: 'Buy Xiaomi Yi Action Camera (Green) from Kogan.com. Shoot crystal clear, ' +
    'Full HD videos with this 16 megapixel action camera. Built-in Wi-Fi for remote ...',
  },
  {
    index: 123,
    url: 'http://www.droneflyers.com/2016/03/xiaomi-yi-action-camera-first-look-and-review/',
    title: 'Xiaomi Yi Action Camera - First Look and Review - Drone Flyers',
    description: 'The Xiaomi Yi is a alternative to the more expensive GoPro cameras and is a ' +
    'very decent all-around sports and action camera for drones and other uses.',
  },
  {
    index: 124,
    url: 'http://www.phonesreview.co.uk/2016/05/06/xiaomi-yi-action-camera-2-leak-reveals-high-' +
    'end-specifications/',
    title: 'Xiaomi Yi Action Camera 2 leak reveals high-end specifications ...',
    description: 'A series of leaked screenshots have spilled the beans on the upcoming Xiaomi ' +
    'Yi Action Camera 2, and it\'s going to be one to watch if you\'re looking for an ...',
  },
  {
    index: 125,
    url: 'https://www.wish.com/c/55890bf1643db33fdf545a5a',
    title: 'Wish | Xiaomi Yi Sport Cam Action Camera Ambarella A7LS WiFi ...',
    description: 'Hottest Xiaomi release Technology Sony Exmor R BSI CMOS 16 Million Pixel ' +
    'Sensor inside Support wireless connection mobile phone, WiFi Connectivity (Up.',
  },
  {
    index: 126,
    url: 'https://www.blackboxmycar.com/products/xiaomi-yi-car-dvr',
    title: 'Xiaomi Yi Car DVR - BlackboxMyCar',
    description: 'The Xiaomi Yi Car DVR is a dashcam with WiFi, 1296P and 1080P recording, ' +
    'and ADAS features.',
  },
  {
    index: 127,
    url: 'http://www.amazon.it/Original-Xiaomi-4608x3456-1920x1080p-Bluetooth/dp/B00UFFZUO6',
    title: 'Original Xiaomi yi Camera Camera Xiaoyi sport di azione 16MP ...',
    description: 'Original Xiaomi yi Camera Camera Xiaoyi sport di azione 16MP 4608x3456 ' +
    '1920x1080p WIFI Bluetooth 4.0 Standard Edition (Bianco): Amazon.it: Elettronica .',
  },
  {
    index: 128,
    url: 'https://shazoo.ru/2016/05/14/39853/xiaomi-yi-4k-action-camera-za-300',
    title: 'Xiaomi Yi 4K Action Camera за $300 - Shazoo',
    description: 'Рынок action-камер возглавляет GoPro, став признанным стандартом. Xiaomi ' +
    'официально представила второе поколение своего решения — Yi 4K ...',
  },
  {
    index: 129,
    url: 'http://www.tudocelular.com/android/noticias/n71772/Nova-camera-Xiaomi-Yi-4k.html',
    title: 'Nova câmera Xiaomi Yi 4k é lançada na China com valor atrativo ...',
    description: 'Dando continuidade a essa linha de câmeras, a companhia anunciou há pouco a' +
    ' Xiaomi Yi 4K, que traz um hardware ainda mais capaz do que os modelos ...',
  },
  {
    index: 130,
    url: 'http://c.mi.com/sg/thread-1282-1-1.html',
    title: '[Unbox + Review + Tips] Super 3-in-1 Xiaomi Yi Action Coffeemix ...',
    description: 'Hi Mi community,. I declare Xiaomi Yi Action camera is my first action camera.' +
    ' Although, I understand in the market, the most popular one is GoPro. Done a bit of ...',
  },
  {
    index: 131,
    url: 'http://tech.thaivisa.com/xiaomi-yi-review/13454/',
    title: 'Xiaomi Yi Review - Tech by Thaivisa.com',
    description: 'Find out about the good and the bad of Xiaomi Yi action camera.',
  },
  {
    index: 132,
    url: 'http://merimobiles.com/original-xiaomi-yi-sport-camera-16mp.htm',
    title: 'Original Xiaomi Yi Sport Camera16MP - Merimobiles',
    description: 'Check out the latest Xiaomi Ants Xiaoyi Smart Camera.',
  },
  {
    index: 133,
    url: 'https://www.quora.com/topic/Xiaomi-Yi-Action-Camera',
    title: 'Xiaomi Yi Action Camera - Quora',
    description: 'How do I take out the battery of the Xiaomi Yi camera when the battery paper' +
    ' is accidentally torn? Philipp Klemm, got the first Chinese version and own the latest ...',
  },
  {
    index: 134,
    url: 'http://xiaomitips.com/guide/how-to-fix-yi-ip-cam-error-5400/attachment/fix-xiaomi-yi-' +
    'ip-cam-error/',
    title: 'Fix Xiaomi Yi IP Cam Error – Xiaomi Tips',
    description: 'Xiaomi Tips. Menu; News · Guide · Download · Devices · All other. Fix Xiaomi' +
    ' Yi IP Cam Error ... I love everything Xiaomi and like collecting useful information.',
  },
  {
    index: 135,
    url: 'http://www.gitup.com/forum/index.php?threads/any-one-knows-xiaomi-yi-action-camera-' +
    'international-version-and-chinese-one.114/',
    title: 'Any one knows Xiaomi yi action camera International version and ...',
    description: 'Hi, i am newbie here! Just want to share with you the difference between' +
    ' Xiaomi yi action camera International version and Chinese version. Any one...',
  },
  {
    index: 136,
    url: 'http://gagadget.com/announce/22053-predstavlena-xiaomi-yi-4k-action-camera-2-s-' +
    'vozmozhnostyu-semki-fullhd-120-kadrov-v-sekundu/',
    title: 'Представлена Xiaomi Yi 4K Action Camera 2 с возможностью ...',
    description: 'Xiaomi представила обновленную экшн-камеру Xiaomi Yi 4K Action Camera 2,' +
    ' о которой практически вся информация была уже известна заранее.',
  },
  {
    index: 137,
    url: 'https://www.amazon.ca/C-Mall-Resistant-Waterproof-Xiaomi-Sports/dp/B00Y1CP6N4',
    title: 'C-Mall Hot/Cold Resistant Waterproof Case for Xiaomi Yi Sports ...',
    description: 'For people who are looking for a mediocre cheap xiaomi yi cam case this ' +
    'would be it. Under certain temperature this camera case function as expected.',
  },
  {
    index: 138,
    url: 'https://www.ktc-ua.com/catalog/photo/ekshn-kamera-xiaomi-yi-sport-whitetravel-154475',
    title: 'Екшн-камера Xiaomi Yi Sport WhiteTravel за 2799.90 грн. | КТС',
    description: 'Хочете купити Екшн-камера Xiaomi Yi Sport WhiteTravel з безкоштовною ' +
    'доставкою в Рівне Тернопіль Луцьк Івано-Франківськ? ☎ (0362) 460246 КТС.',
  },
  {
    index: 139,
    url: 'http://www.funkykit.com/reviews/accessory/xiaomi-yi-cam/',
    title: 'Xiaomi Yi Cam - A Very Affordable IP Camera - Funky Kit',
    description: 'I first stumbled on Xiaomi\'s Yi Cam when I wrote a quick company profile' +
    ' a few months back. Being priced at just RMB149, it was a small cheap gadget that I had ...',
  },
  {
    index: 140,
    url: 'http://creativedex.com/products/xiaomi-yi-mount',
    title: 'Xiaomi Yi Mount – Creative Dex!',
    description: 'Every FPV \'fighter pilot\' quickly learns about the mixed relationship' +
    ' we have with Terra Firma. The harder we try to leave her, the more tenaciously she\'ll grip.',
  },
  {
    index: 141,
    url: 'http://extremehobbiesmiami.com/xiaomi-yi-action-camera.html',
    title: 'Xiaomi Yi Sport Camera (Xiaomi Yi Action Camera)',
    description: 'It supports 64GB SD card as the saving equipment. With easy to use and ' +
    'high performance, it is really a wonderful action camcorder for you. Xiaomi Yi Action ...',
  },
  {
    index: 142,
    url: 'https://www.behance.net/gallery/28827053/LP-Web-Design-Programming-Xiaomi-YI-Action-' +
    'Camera',
    title: 'LP - Web Design & Programming Xiaomi YI - Action Camera on ...',
    description: 'Xiaomi YI - action camera - landing page. ... LP - Web Design & Programming ' +
    'Xiaomi YI - Action Camera. Web Design. 2947. 396. 12. Ivan Konkov · Moscow ...',
  },
  {
    index: 143,
    url: 'http://www.adslzone.net/2016/05/12/comparativa-xiaomi-yi-4k-vs-gopro-hero-4-bateria' +
    '-estabilizacion-video/',
    title: 'Comparativa Xiaomi Yi 4K vs GoPro Hero 4: batería y estabilización ...',
    description: 'Comparativa en vídeo de la duración de la batería y la estabilización del' +
    ' vídeo que ofrecen la Yi 4K Camera de Xiaomi y la GoPro Hero 4.',
  },
  {
    index: 144,
    url: 'http://panotek360rigs.com/',
    title: 'Panotek 360 Rigs – 360 Rig for Xiaomi Yi',
    description: '360 RIG AND XIAOMI YI CAMERAS UNDER €800. · FREE SHIPPING WORLDWIDE ·.' +
    ' Kraken360 Rig. A 360 RIG SPECIALLY DESIGNED FOR XIAOMI YI.',
  },
  {
    index: 145,
    url: 'https://forums.oculus.com/vip/discussion/27189/update-on-sub-600-complete-xiaomi-yi-' +
    '360-camera-rig',
    title: 'Update on sub $600 complete Xiaomi Yi 360 camera Rig - Oculus',
    description: 'I thought I would post an update video on my work with 360 rigs to show the' +
    ' progress and with a sample, please remember to max out the video quality in the ...',
  },
  {
    index: 146,
    url: 'http://qway.com.ua/cn-xiaomi-yi',
    title: 'Камера Xiaomi Yi Sport Camera - купить в Украине (z22, z22l,z23l)',
    description: 'Камера Xiaomi Yi Basic (1080p, 60fps, Wi-Fi). 2 025 грн / $74 . ' +
    'Характеристики. Фото. Отзывы. Зачем покупать на olx/slando/prom/aukro?',
  },
  {
    index: 147,
    url: 'http://www.spidersweb.pl/2016/05/xiaomi-yi-ii-4k.html',
    title: 'Kamera sportowa Xiaomi Yi II już dostępna. Specyfikacja jest ...',
    description: 'Kamerka sportowa Xiaomi Yi (znana też pod nazwą Xiaomi Action Cam) była ' +
    'prawdziwym hitem. Teraz do sklepów trafia jej następczyni, Xiaomi Yi 2, ...',
  },
  {
    index: 148,
    url: 'https://www.vrideo.com/watch/zYz0HsW',
    title: 'Entaniya Fisheye 250 + modified xiaomi Yi 360 Video | Vrideo',
    description: 'Entaniya Fisheye 250 + modified xiaomi Yi x 2 mode 4:3 Back to Back (both ' +
    'holizontal style) □Entaniya Fisheye Web https://www.entapano.com/en/l/index. html ...',
  },
  {
    index: 149,
    url: 'https://www.ipcamtalk.com/showthread.php/6845-Has-anyone-managed-to-set-up-Xiaomi-Y' +
    'i-IP-camera-with-Blue-Iris',
    title: 'Has anyone managed to set up Xiaomi Yi IP camera with Blue Iris ...',
    description: 'Any instructions would be much appreciated. I have just bought this camera a' +
    'nd would like to learn how to set it up before its delivered. I have seen in a few ...',
  },
  {
    index: 151,
    url: 'http://forum.supercell.net/showthread.php/1132004-Xiaomi-Yi-Action-Camera-2-features',
    title: 'Looking for a Task Force Xiaomi Yi Action Camera 2 features ...',
    description: 'Xiaomi Yi Action Camera 2 launched With 4K Video Support and also sports 1' +
    '2- megapixel Sony IMX377 sensor . watch Captain America: Civil War ...',
  },
  {
    index: 152,
    url: 'http://www.getfpv.com/electronics/hd-cameras/xiaomi-yi-action-camera-1080p-60fps-black' +
    '.html',
    title: 'the Xiaomi Yi Action Camera - GetFPV',
    description: 'The Xiaomi Yi action camera is a 1080P 60FPS action camera with many features ' +
    'like Wifi, Bluetooth and more. The Yi has a similar form factor as the GoPro ...',
  },
  {
    index: 153,
    url: 'http://www.otismaxwell.com/blog/making-the-most-of-your-xiaomi-yi-camera/',
    title: 'Otis Regrets… or Not › Making the most of your Xiaomi Yi camera',
    description: 'It\'s oddly hard to find good info online about the extensive capabilities' +
    ' of the Xiaomi Yi camera. Here are a few random tips which I recorded simply because I ...',
  },
  {
    index: 154,
    url: 'http://fpv-reconn.com/store/index.php?route=product/product&product_id=151',
    title: 'Xiaomi Yi Casing (Free Download / 3D Print) - FPV-Reconn.com',
    description: 'This casing is specially designed by FPV-Reconn\'s designer Trinco for XiaoMi ' +
    'Yi Action Camera. The files are available for free in the download section below.',
  },
  {
    index: 155,
    url: 'http://www.dx.com/p/pannovo-battery-back-cover-dust-cover-for-xiaomi-yi-action-camera' +
    '-white-392882',
    title: 'PANNOVO Battery Back Cover + Dust Cover for Xiaomi Yi Action ...',
    description: 'Only US$2.39, buy PANNOVO Battery Back Cover + Dust Cover for Xiaomi Yi Action' +
    ' Camera - White from DealExtreme with free shipping now.',
  },
  {
    index: 156,
    url: 'http://www.nextbuying.com/official-waterproof-case-for-xiaomi-yi-action-camera-diving' +
    '-40m-p000335.html',
    title: 'Official Original Waterproof Case for Xiaomi Yi Action Camera ...',
    description: 'Official Original Xiaomi Waterproof Case for Xiaomi Yi Action Camera Diving ' +
    '40M , Best price xiaomi Dive Housing Yi sports cam white & green color at ...',
  },
  {
    index: 157,
    url: 'http://price.ua/xiaomi/xiaomi_yi_sport_white_basic_edition/catc7569m1089360.html',
    title: 'Xiaomi Yi Sport White Basic Edition: цены в Украине. Купить ...',
    description: 'Цены на Xiaomi Yi Sport White Basic Edition от 1 987 грн. до 2 399 грн. в инт' +
    'ернет-магазинах Украины на PRICE.UA. Характеристики Xiaomi Yi Sport ...',
  },
  {
    index: 158,
    url: 'http://www.paraglidingforum.com/viewtopic.php?t=73007',
    title: 'Paragliding Forum - View topic - Xiaomi YI 1080p 60 fps Awesome - new',
    description: 'I think that NOW the action camera market IS mature, at least for my needs, I' +
    ' just bought a Xiaomi YI camera for 77 USD including shipping from gearbest,',
  },
  {
    index: 159,
    url: 'https://blog.adafruit.com/2016/03/03/neopixel-ring-light-for-xiaomi-yi/',
    title: 'NeoPixel Ring Light for XiaoMi Yi « Adafruit Industries – Makers ...',
    description: 'March 3, 2016 AT 5:00 pm. NeoPixel Ring Light for XiaoMi Yi. xiaomi-yi-neopixel' +
    '- ring bobthesheep shared on Thingiverse: NeoPixel Ring Light for XiaoMi Yi.',
  },
  {
    index: 160,
    url: 'http://a-onetechnology.manufacturer.globalsources.com/si/6008850457998/pdtl/Waterproof' +
    '-bullet/1138663693/Xiaomi-Yi-Macro-Lens-%2012.5.htm',
    title: 'China Xiaomi Yi Macro Lens +12.5 from Shenzhen Manufacturer ...',
    description: 'Xiaomi Yi Macro Lens +12.5 Xiaomi Yi Macro Lens • Min. Order: 500 Sets • FOB P' +
    'rice: US$ 6.2 - US$ • supplied by Shenzhen A-One Technology Industry Co., ...',
  },
  {
    index: 161,
    url: 'https://uavsystemsinternational.com/product/xiaomi-yi-action-camera/',
    title: 'Xiaomi Yi Action Camera | UAV Systems International',
    description: 'Discover the Xiaomi Yi camera, an excellent alternative to the GoPro for mounti' +
    'ng on your drone at a fraction of the price.',
  },
  {
    index: 162,
    url: 'http://forums.crackberry.com/blackberry-classic-f419/xiaomi-yi-chinese-gopro-1023384/',
    title: 'Xiaomi Yi (Chinese GoPro) - BlackBerry Forums at CrackBerry.com',
    description: 'Hey, Has anyone tried to download the Yi app on their Classic and connecting i' +
    't to their Yi camera ? Any luck ? If you come across some info about this, I would ...',
  },
  {
    index: 163,
    url: 'http://www.yeggi.com/q/xiaomi+yi/2/',
    title: '\'xiaomi yi\' 3D Models to Print - yeggi - page 2',
    description: '506 \'xiaomi yi\' 3D Models. Every Day new 3D Models from all over the World' +
    '. Click to find the best Results for xiaomi yi Models for your 3D Printer.',
  },
  {
    index: 164,
    url: 'https://news.google.co.in/news/more?ncl=dzYMPBl_-hx4a5MITPqrdYXM1AuWM&authuser=0&ned' +
    '=in&topic=tc',
    title: 'Xiaomi Yi 4K action camera with 12MP... - Google News',
    description: 'Xiaomi has announced the upgrade to its Yi Action Camera 2 – called the Yi 4K' +
    '. As the name suggests, it can shoot 4K videos at 30fps, along with full HD ...',
  },
  {
    index: 165,
    url: 'http://news.google.com.au/news/more?ncl=dzYMPBl_-hx4a5MITPqrdYXM1AuWM&authuser=0&ned' +
    '=au&topic=tc',
    title: 'Xiaomi Yi 4K action camera with 12MP... - Google News',
    description: 'Xiaomi has announced the upgrade to its Yi Action Camera 2 – called the Yi 4K.' +
    ' As the name suggests, it can shoot 4K videos at 30fps, along with full HD ...',
  },
  {
    index: 166,
    url: 'http://www.bgr.in/news/xiaomi-yi-4k-action-camera-with-2-19-inch-display-announced-i' +
    'n-china-for-200-specifications-and-features/',
    title: 'Xiaomi Yi 4K Action Camera with 2.19-inch display announced in ...',
    description: 'Xiaomi\'s take on GoPro, the Yi Cam, now comes with 4K video recording capabil' +
    'ities at 30fps and improved hardware. - Xiaomi Yi 4K Action Camera with ...',
  },
  {
    index: 167,
    url: 'http://www.fredzone.org/xiaomi-yi-4k-une-nouvelle-concurrente-de-taille-pour-les-gopr' +
    'o-665',
    title: 'Xiaomi YI 4K : une nouvelle concurrente de taille pour les GoPro',
    description: 'Elle vient de remettre le couvert avec la Xiaomi Yi 4K, une caméra qui risque' +
    ' de coller des sueurs froides à GoPro et à tous les constructeurs positionnés sur ce ...',
  },
  {
    index: 168,
    url: 'http://altadefinizione.hdblog.it/2016/05/12/Xiaomi-Yi-4K-Action-Camera-ufficiale-e-' +
    'piu-cara/',
    title: 'Xiaomi Yi 4K Action Camera ufficiale: migliore della precedente ma ...',
    description: 'Ecco dunque la nuova Xiaomi Yi 4K Action Camera, decisamente più rifinita del' +
    'la prima, utilizzabile in maniera indipendente grazie al display montato sulla ...',
  },
  {
    index: 169,
    url: 'http://www.xatakafoto.com/actualidad/xiaomi-yi-4k-como-de-buena-es-la-camara-de-ac' +
    'cion-barata-de-los-chinos',
    title: 'Xiaomi Yi 4K: ¿cómo de buena es la cámara de acción barata de los ...',
    description: 'Descripción y detalles sobre la Xiaomi Yi 4K, nueva cámara de acción barata' +
    ' de origen chino que viene a rivalizar con las GoPro...',
  },
  {
    index: 170,
    url: 'http://www.thequint.com/technology/2016/05/14/xiaomis-yi-action-camera-2-gives-gopro' +
    '-hero-4-a-run-for-its-money',
    title: 'Xiaomi\'s Yi Action Camera 2 Gives GoPro Hero 4 a Run for Its ...',
    description: 'But that\'s not all – Xiaomi has served the adventure purists with its own ve' +
    'rsion of an action camera, called Yi Action camera. And just like every other product in ...',
  },
  {
    index: 171,
    url: 'http://xiaomiadvices.com/xiaomi-yi-4k-action-camera-2-buy-online/',
    title: 'GearBest: Pre-order Xiaomi Yi 4K Action Camera 2 for just $249.99 ...',
    description: 'Xiaomi Yi 4K Action Camera 2 is the best sports camera with 4K video recor' +
    'ding - Buy online, coupons, features, review, GearBest.',
  },
  {
    index: 172,
    url: 'https://www.nextpowerup.com/news/28061/xiaomi-yi-4k-camera-now-officially-announced/',
    title: 'Xiaomi YI 4K Camera Now Officially Announced | NextPowerUp',
    description: 'Although the Xiaomi YI 4k camera was already listed by BangGood, the compa' +
    'ny has officially announced the camera today. It comes in Black, White and Pink ...',
  },
  {
    index: 173,
    url: 'http://mymathforum.com/algebra/331590-xiaomi-yi-action-camera-2-features.html',
    title: 'Xiaomi Yi Action Camera 2 features. - My Math Forum',
    description: 'Xiaomi Yi Action Camera 2 launched With 4K Video Support and also sports ' +
    '12- megapixel Sony IMX377 sensor . watch Captain America: Civil War Online.',
  },
  {
    index: 174,
    url: 'http://www.smartworld.it/tecnologia/xiaomi-yi-4k-camera-caratteristiche-uscita-prezz' +
    'o.html',
    title: 'Xiaomi Yi 4K action cam: caratteristiche, uscita e prezzo | SmartWorld',
    description: 'Xiaomi Yi 4K è la nuova action cam con schermo touch e registrazione fino in' +
    ' 4K @30fps che punta al mercato top: ecco tutto quello che dovete sapere.',
  },
  {
    index: 175,
    url: 'http://hitech.newsru.com/article/13may2016/yi4k',
    title: 'Xiaomi анонсировала второе поколение экшн-камеры Yi',
    description: 'В марте прошлого года китайская компания Xiaomi представила недорогую экш' +
    'н-камеру Yi, которая отличалась хорошим соотношением характеристик ...',
  },
  {
    index: 176,
    url: 'http://androidportal.zoznam.sk/2016/05/xiaomi-yi-2-4k-kamera-200-eur/',
    title: 'Xiaomi Yi 2 oficiálne: 4K kamera za cenu do 200€! - AndroidPortal.sk',
    description: 'Spoločnosť Xiaomi ešte minulý rok s Xiaomi Yi vstúpila do segmentu akčným ' +
    'kamier. Okamžite si ju obľúbili milióny ľudí po celom svete. Milá akčná kamera ...',
  },
  {
    index: 177,
    url: 'http://fitnhit.com/news/xiaomi-yi-action-camera-2-launched-with-4k-video-support-at' +
    '-rs-12000/69970/',
    title: 'Xiaomi Yi Action Camera 2 Launched with 4K Video Support at Rs ...',
    description: 'Xiaomi Yi Action Camera 2: The market that was earlier dominated by GoPro l' +
    'ast year with a new launch called Yi Action Camera at a very cheaper price Rs ...',
  },
  {
    index: 178,
    url: 'http://www.zyngaplayerforums.com/showthread.php?3166363-Xiaomi-Yi-Action-Camera-2-fea' +
    'tures',
    title: 'Xiaomi Yi Action Camera 2 features. - Zynga Player Forums',
    description: 'Xiaomi Yi Action Camera 2 launched With 4K Video Support and also sports 12- ' +
    'megapixel Sony IMX377 sensor . watch Captain America: Civil War ...',
  },
  {
    index: 179,
    url: 'http://www.whatsontech.com/2016/05/14/xiaomi-yi-4k-action-camera-launched-china-spe' +
    'cs-price-here-is-how-to-buy-one/29141/',
    title: 'Xiaomi Yi 4K Action Camera launched in China for $200',
    description: 'The Xiaomi Yi 4K action camera will feature a 12.3MP Sony IMX377 sensor wit' +
    'h f /2.8 aperture and 160-degree wide angle lens, which also supports 720p (Max ...',
  },
  {
    index: 180,
    url: 'http://www.mobiflip.de/xiaomi-yi4k-action-kamera-uhd/',
    title: 'Xiaomi Yi 4K: Action-Kamera mit UHD-Video und Touchscreen ...',
    description: 'Bislang hatte der chinesische Hersteller Xiaomi mit der Yi Cam eine günstig' +
    'e Action-Kamera im Angebot, welche lediglich rund 60 Euro ...',
  },
  {
    index: 181,
    url: 'http://www.themarshalltown.com/2016/05/14/xiaomi-to-take-on-gopro-with-yi-4k-acti' +
    'on-camera-2-lower-price-point-with-similar-features/',
    title: 'Xiaomi To Take On GoPro With Yi 4K Action Camera 2: Lower Price ...',
    description: 'Xiaomi, one of China\'s biggest electronics companies and the fifth larges' +
    't smartphone maker in the world, has announced that its new Yi 4K Action Camera 2 is ...',
  },
  {
    index: 182,
    url: 'http://www.tabletowo.pl/2016/05/14/xiaomi-yi-ii-z-nagrywaniem-w-4k-i-dotykowym-ekranem/',
    title: 'Xiaomi Yi II z nagrywaniem w 4K i dotykowym ekranem – nowość w ...',
    description: 'Jakiś czas temu do oferty firmy dołączyła nowa wersja kamerki sportowej, ob' +
    'ok której naprawdę trudno przejść obojętnie. Xiaomi Yi II, bo o niej mowa, jest już ...',
  },
  {
    index: 183,
    url: 'http://www.loveshack.org/forums/transitioning/friends-lovers/581091-xiaomi-yi-action' +
    '-camera-2-features',
    title: 'Xiaomi Yi Action Camera 2 features. - LoveShack.org Community Forums',
    description: 'Xiaomi Yi Action Camera 2 launched With 4K Video Support and also sports 12-' +
    ' megapixel Sony IMX377 sensor . watch Captain America: Civil War Online.',
  },
  {
    index: 184,
    url: 'https://www.mojandroid.sk/xiaomi-yi-action-camera-4k-predstavena/',
    title: 'Xiaomi Yi Action Camera 4K parametrami poráža aj GoPro',
    description: 'Xiaomi v tichosti predstavil svoju novú akčnú kameru Yi. Tá vo svojej cenov' +
    'ej kategórii ponúka nadštandardnú výbavu, ktorou poráža aj GoPro Hero4 Black.',
  },
  {
    index: 185,
    url: 'http://refresher.sk/33391-Dockali-sme-sa-Akcna-kamera-Xiaomi-Yi-2-prinasa-4K-videa-le' +
    'psiu-vydrz-a-uplne-novy-dizajn',
    title: 'Dočkali sme sa. Akčná kamera Xiaomi Yi 2 prináša 4K videá, lepšiu ...',
    description: 'Dočkali sme sa. Akčná kamera Xiaomi Yi 2 prináša 4K videá, lepšiu výdrž a úp' +
    'lne nový dizajn. A aby toho nebolo málo, tešiť sa môžeme aj na dotykový displej.',
  },
  {
    index: 186,
    url: 'http://www.bikeforums.net/introductions/1063445-xiaomi-yi-action-camera-2-features.html',
    title: 'Xiaomi Yi Action Camera 2 features. - Bike Forums',
    description: 'Xiaomi Yi Action Camera 2 launched With 4K Video Support and also sports 12-' +
    ' megapixel Sony IMX377 sensor . watch Captain America: Civil War Online.',
  },
  {
    index: 187,
    url: 'http://mathhelpforum.com/new-users/261511-xiaomi-yi-action-camera-2-features.html',
    title: 'Xiaomi Yi Action Camera 2 features. - Math Help Forum',
    description: 'Xiaomi Yi Action Camera 2 launched With 4K Video Support and also sports 12-' +
    ' megapixel Sony IMX377 sensor . watch Captain America: Civil War Online.',
  },
  {
    index: 188,
    url: 'http://www.tgspot.co.il/xiaomi-yi-4k-action-camera-launche/',
    title: 'שאומי הכריזה על מצלמת אקסטרים Xiaomi Yi 4K Action - TGspot',
    description: 'שאומי (Xiaomi) נכנסה לא מכבר לתחום מצלמות האקסטרים עם Xiaomi Yi Cam בתג מחיר נמ' +
    'וך במיוחד שאותה כבר סקרנו אצלנו וכעת היא מנסה את מזלה שוב, הפעם הם מצלמה חדש.',
  },
  {
    index: 189,
    url: 'http://www.tuttoandroid.net/xiaomi/xiaomi-yi-4k-action-camera-2-ufficiale-xiaomi-m' +
    'i-band-2-giugno-376002/',
    title: 'Xiaomi Yi 4K Action Camera 2 è ufficiale, Xiaomi Mi Band 2 arriverà ...',
    description: 'Sono passati pochi giorni, meno di una settimana, da quando sono trapelate' +
    ' in rete le prime informazioni relative a Xiaomi Yi 4K Action Camera 2 che oggi è ...',
  },
  {
    index: 190,
    url: 'http://www.techandroids.com/xiaomi-yi-4k-action-camera-announced-comes-12mp-sony-sensor/',
    title: 'Xiaomi YI 4K action camera announced, comes with 12MP Sony ...',
    description: 'Xiaomi YI has announced its latest 4K action camera. It has 12MP Sony IMX' +
    '377 sensor and supports Hyberlapse as well. Other camera features include ...',
  },
  {
    index: 191,
    url: 'http://www.miscw.com/xiaomi-launched-yi-action-camera-2-4k-support-7301.html',
    title: 'Xiaomi launched Yi Action Camera 2 with 4K support',
    description: 'Xiaomi launched Yi Action Camera 2 with 4K support in China for RMB 1199' +
    ' ( approx Rs.12200). In terms of Specification and features, Xiaomi Yi Action Camer .',
  },
  {
    index: 192,
    url: 'http://timesofindia.indiatimes.com/tech/more-gadgets/Xiaomi-unveils-Yi-Action-Cam' +
    'era-2-with-4K-support/articleshow/52261256.cms',
    title: 'Xiaomi unveils Yi Action Camera 2 with 4K support - Times of India',
    description: 'NEW DELHI: Chinese electronics startup Xiaomi has announced the successor' +
    ' of its GoPro-like Yi action camera. The Yi Action Camera 2 features better ...',
  },
  {
    index: 225,
    url: 'http://sportscar365.com/features/videos/video-silverstone-race-highlights-5/',
  },
  {
    index: 274,
    url: 'http://www.touringcartimes.com/2016/04/11/polestar-cyan-racings-first-wtcc-race-of-2016/',
  },
  {
    index: 300,
    url: 'http://censor.net.ua/video_news/367426/znakovoe_i_samoe_zapominayuscheesya_video_201' +
    '5_goda_po_versii_tsenzornet_video',
  },
];
