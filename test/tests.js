/* eslint-env: mocha */
/* eslint strict: 0 */
/* eslint no-undef: 0 */
/* eslint arrow-body-style: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint global-require: 0 */
'use strict';

const _ = require('lodash');
const chai = require('chai');
const PageAnalysis = require('../helpers/page-analysis');

chai.use(require('chai-things'));
chai.use(require('chai-as-promised'));
chai.should();

suite('Page Analysis class', () => {
  suite('.fetch()', () => {
    let links;
    suiteSetup(() => {
      links = {
        empty: '',
        pdf: 'http://www.arvindguptatoys.com/arvindgupta/zenmind.pdf',
        doc: 'https://hostmaster.ua/policy/Policy_of_.UA.doc',
        ppt: 'https://acdbio.com/sites/default/files/sample.ppt',
        pptx: 'https://acdbio.com/sites/default/files/sample.pptx',
        cutestat: 'https://www.crunchbase.com/organization/cutestat#/entity',
        clearwebstat: 'https://who.is/nameserver/ns1.clearwebstats.com/',
        download: 'https://www.skype.com/ru/download-skype/skype-for-computer/',
        cv: 'https://www.start.cv.ua/',
        timeout: 'https://pravda.com.ua',
        normal: 'http://usejsdoc.org/tags-function.html',
      };
    });

    test('should reject on empty urls', () => {
      return PageAnalysis.fetchPage('').should.eventually.be
        .rejectedWith('@PAGE-INITIALIZATION :: Invalid url -> ');
    });

    test('should reject on links to pdf-documents', () => {
      return PageAnalysis.fetchPage(links.pdf)
        .should.eventually.be.rejectedWith(`@PAGE-INITIALIZATION :: Invalid url -> ${links.pdf}`);
    });

    test('should reject on links to doc-documents', () => {
      return PageAnalysis.fetchPage(links.doc)
        .should.eventually.be.rejectedWith(`@PAGE-INITIALIZATION :: Invalid url -> ${links.doc}`);
    });

    test('should reject on links to ppt-documents', () => {
      return PageAnalysis.fetchPage(links.ppt)
        .should.eventually.be.rejectedWith(`@PAGE-INITIALIZATION :: Invalid url -> ${links.ppt}`);
    });

    test('should reject on links to pptx-documents', () => {
      return PageAnalysis.fetchPage(links.pptx)
        .should.eventually.be.rejectedWith(`@PAGE-INITIALIZATION :: Invalid url -> ${links.pptx}`);
    });

    test('should reject on links leading to cutestat', () => {
      return PageAnalysis.fetchPage(links.cutestat).should.eventually.be
        .rejectedWith(`@PAGE-INITIALIZATION :: Invalid url -> ${links.cutestat}`);
    });

    test('should reject on links leading to clearwebstat', () => {
      return PageAnalysis.fetchPage(links.clearwebstat).should.eventually.be
        .rejectedWith(`@PAGE-INITIALIZATION :: Invalid url -> ${links.clearwebstat}`);
    });

    test('should reject on download links', () => {
      return PageAnalysis.fetchPage(links.download).should.eventually.be
        .rejectedWith(`@PAGE-INITIALIZATION :: Invalid url -> ${links.download}`);
    });

    test('should reject on *.cv.ua links', () => {
      return PageAnalysis.fetchPage(links.cv)
        .should.eventually.be.rejectedWith(`@PAGE-INITIALIZATION :: Invalid url -> ${links.cv}`);
    });

    test('should reject on on timeout', () => {
      return PageAnalysis.fetchPage(links.timeout).should.eventually;
    });

    test('should extract page body', () => {
      return PageAnalysis.fetchPage(links.normal).should.eventually
        .have.a.property('body').that.is.a('string').that.contains('<html');
    });

    test('should extract page response headers', () => {
      return PageAnalysis.fetchPage(links.normal).should.eventually
        .have.a.property('headers').that.is.an('object');
    });
  });

  suite('.checkLanguage()', () => {
    test('should process array of keywords', () => {
      const keywords = ['Tartu', 'ja', 'Pärnu', 'esinduste', 'juurde', 'on', 'lisandunud', 'Jõhvi',
        'esindus', 'Tutvuge', 'meie', 'teenuste', 'esinduste', 'ja', 'avamisaegadega', 'vajutades',
        'lingile'];
      return PageAnalysis.checkLanguage(keywords).should.eventually.eql('et');
    });

    test('should process a chunk of text', () => {
      const phrase = 'When both all and have are used, the target object must both contain' +
        ' all of the passed-in keys AND the number of keys in the target object must match the' +
        ' number of keys passed in (in other words, a target object must have all and only all ' +
        'of the passed-in keys).';

      return PageAnalysis.checkLanguage(phrase).should.eventually.eql('en');
    });

    test('should process empty array of keywords', () => {
      return PageAnalysis.checkLanguage([]).should.eventually.eql(null);
    });

    test('should process an empty chunk of text', () => {
      return PageAnalysis.checkLanguage('').should.eventually.eql(null);
    });

    test('shouldn\'t fail on no incoming chunks', () => {
      return PageAnalysis.checkLanguage('').should.eventually.eql(null);
    });
  });

  suite('#trendsParser()', () => {
    const body = require('./trends-response-test');

    test('should correctly retrieve Google Trends data', () => {
      return PageAnalysis._trendsParser({ body }).should.eventually.be.an('array');
    });

    test('each response item should contain `value` property', () => {
      return PageAnalysis._trendsParser({ body }).should.eventually.all.contain.keys('value');
    });

    test('should correctly reject on Google Trends server errors', () => {
      return PageAnalysis._trendsParser({ body: 'Data table response Internal Server Error' })
        .should.eventually.be.rejectedWith('Trends extraction fail');
    });

    test('should correctly reject on Google Trends server errors', () => {
      return PageAnalysis._trendsParser({ body: 'Data Not enough search volume to show results' })
        .should.eventually.be.rejectedWith('Trends extraction fail');
    });

    test('should correctly reject on Google Trends server errors', () => {
      return PageAnalysis._trendsParser({ body: 'Data table You have reached your quota limit' })
        .should.eventually.be.rejectedWith('Google ban');
    });
  });

  suite.skip('.getTrends()', () => {
    test('should correctly retrieve Google Trends data', () => {
      return PageAnalysis.getTrends('Max Verstappen').should.eventually.be.an('array');
    });

    test('each response item should contain `value` property', () => {
      return PageAnalysis.getTrends('Max Verstappen', 'nl')
        .should.eventually.all.contain.keys('value');
    });
  });

  suite('#evalRelevanceRate()', () => {
    let analysis;
    suiteSetup(() => {
      analysis = new PageAnalysis({ query: 'Max Verstappen', keys: [], pages: [] });
    });

    test('should evaluate correct relevancy rate value', () => {
      return analysis._evalRelevanceRate([
        { value: 100 }, { value: 85 }, { value: 76 }, { value: 64 }, { value: 98 },
      ]).should.eventually.be.within(0, 100);
    });
  });

  suite('#findVendorComments()', () => {});

  suite('#findInternalComments()', () => {});

  suite.only('.leadsScan()', () => {
    let analysis;
    suiteSetup(() => {
      analysis = new PageAnalysis({
        pages: require('../test/google-response-test'),
        query: 'Xiaomi Yi',
        keys: ['Xiaomi Yi', 'video', 'camera', 'HD'],
        inclusions: ['yi, video cam', 'cam', 'fullhd'],
        exclusions: ['pantech', 'pentax', 'GoPro hero', 'Don\'t be a hero'],
      });
    });

    test('should return valid URL within `link` field', () => {
      analysis.leadsScan()
        .then(results => {
          _.forEach(results, r => r.link.should.be.a('string'));
        });
    });

    test('should return language of the webpage', () => {
      return analysis.leadsScan()
        .then(results => {
          _.forEach(results, r => r.language.should.match(/[a-z]{2}/));
        });
    });

    test('should return Alex Rank value of the webpage', () => {
      analysis.leadsScan()
        .then(results => {
          _.forEach(results, r => r.info.rank.should.be.a('number'));
        });
    });

    test('should return potency rate of the webpage', () => {
      return analysis.leadsScan()
        .then(results => {
          _.forEach(results, r => r.potentialRate.should.be.a('number').that.is.within(10, 101));
        });
    });

    test('should return accuracy rate of the webpage', () => {
      return analysis.leadsScan()
        .then(results => {
          _.forEach(results, r => r.accuracyRate.should.be.a('number').that.is.within(10, 101));
        });
    });

    test('should return comment boxes detected of the webpage', () => {
      return analysis.leadsScan().should.eventually.all.have.a.property('comments')
        .that.has.a.length;
    });

    test('should return title of the webpage', () => {
      return analysis.leadsScan()
        .then(results => {
          _.forEach(results, r => r.title.should.be.a('string').that.has.length.of.within(1, 141));
        });
    });
  });
});
